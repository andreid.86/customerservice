﻿using Core.Enums;

namespace Core
{
    public static class Constants
    {

        public static class Application
        {
            public const string Name = "Customer Service";
            public const string CultureInfo = "ru-RU";
        }

        public static class WebNames
        {
            public const string MainAppJsBundleName = "CustomerServiceApp";
            public const string CssBundleName = "css-bundle";
            public const string JQueryBundleName = "jquery";
            public const string JQueryValBundleName = "jqueryval";
            public const string ModernizrBundleName = "modernizr";
            public const string BootstrapJsBundleName = "bootstrap";
            public const string AngularJsBundleName = "angularjs";
            public const string AngularUi = "angular-ui";
            public const string Toastr = "angular-toastr";
        }

        public static class DataContext
        {
            public const string Name = "CustomerServiceContext";
            public const int CommandTimeout = 600;
        }

        public static class Identity
        {
            public const bool AllowOnlyAlphanumericUserNames = false;
            public const bool RequireUniqueEmail = true;

            // Account lockout policies
            public const int DefaultAccountLockoutTimeSpanMinutes = 5;
            public const int MaxFailedAccessAttemptsBeforeLockout = 5;
            public const bool UserLockoutEnabledByDefault = false;

            // Passwords policies
            public const int RequiredPasswordLength = 6;
            public const bool RequireNonLetterOrDigit = false;
            public const bool RequireDigit = false;
            public const bool RequireLowercase = false;
            public const bool RequireUppercase = false;

            public const string DefaultUserDomain = "domain.org";
            public const string DefaultUserPassword = "passw0rd";

            public const string AnonymousUserName = "Anonymous";
        }

        public static class RoleNames
        {
            public const string Administrator = "admin";
            public const string Api = "api";
        }


        public static class TableNames
        {
            public static readonly string RequestsTableName = "Requests";
            public const string UserStatusesTableName = "UserStatuses";
            public const string RequestStatusesTableName = "RequestStatuses";
            public const string UserTypesTableName = "UserTypes";
            public const string UserClaimsTableName = "UserClaims";
            public const string UserRolesTableName = "UserRoles";
            public const string RolesTableName = "Roles";
            public const string UserLoginsTableName = "UserLogins";
            public const string UsersTableName = "Users";
        }

        public static class UserStatuses
        {
            public static int DefaultUserStatus = (int) UserStatus.Free;
        }

        public static class UserTypes
        {
            public static readonly string DefaultUserType = UserType.Customer.ToString();
            public static readonly string AdminUserType = UserType.Admin.ToString();
        }

        public static class RequestStatuses
        {
            public static int DefaultRequestStatus = (int) RequestStatus.New;
        }

        public static class Parameters
        {
            public const string WarningRequestTimeThresholdSeconds = "WarningRequestTimeThresholdSeconds";
            public const string CriticalRequestTimeThresholdSeconds = "CriticalRequestTimeThresholdSeconds";
            public const string LowerClientRequestRuntimeRangeSeconds = "LowerClientRequestRuntimeRangeSeconds";
            public const string UpperClientRequestRuntimeRangeSeconds = "UpperClientRequestRuntimeRangeSeconds";
            public const string RequestsQueueProcessingTimeoutSeconds = "RequestsQueueProcessingTimeoutSeconds";
            public const string FinalizerLoopServiceTimeoutSeconds = "FinalizerLoopServiceTimeoutSeconds";
        }

        public static class DateTime
        {
            public const int SecondsPerMinute = 60;
            public const int MillisecondsPerSecond = 1000;
        }

    }
}
