﻿namespace Core.Enums
{
    public enum UserType : int
    {
        Admin = 1,
        Support = 2,
        Manager = 3,
        Executive = 4,
        Customer = 5,
    }
}
