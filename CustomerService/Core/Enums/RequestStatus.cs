﻿namespace Core.Enums
{
    public enum RequestStatus : int
    {
        New = 1,
        InProgress = 2,
        Closed = 3
    }
}