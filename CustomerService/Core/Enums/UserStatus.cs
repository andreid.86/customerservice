﻿namespace Core.Enums
{
    public enum UserStatus : int
    {
        Free = 1,
        Busy = 2,
        Away = 3
    }
}