﻿using Core.Utils.Extensions;
using System;

namespace Core.Utils
{
    public static class Randoms
    {
        public static string ApiKey => Guid.NewGuid().ToString().GetMD5();

        public static int RandomBetween(int first, int second)
        {
            var random = new Random();
            var max = Math.Max(first, second);
            var min = Math.Min(first, second);

            return random.Next(min, max);
        }
    }
}