﻿using System.Text;

namespace Core.Utils.Extensions
{
    public static class StringExtensions
    {
        public static string GetMD5(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return string.Empty;

            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(text);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                var sb = new StringBuilder();
                foreach (var t in hashBytes)
                {
                    sb.Append(t.ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}