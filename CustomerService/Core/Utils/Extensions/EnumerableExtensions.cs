﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Utils.Extensions
{
    public static class EnumerableExtensions
    {
        private static readonly Random Rnd = new Random();

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var item in enumerable)
            {
                action(item);
            }
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            switch (enumerable)
            {
                case null:
                    return true;
                case ICollection<T> collection:
                    return collection.Count < 1;
                default:
                    return !enumerable.Any();
            }
        }

        public static T TakeRandom<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null) 
                return default(T);
            var array = enumerable as T[] ?? enumerable.ToArray();
            var index = Rnd.Next(0, array.Count());
            return array.ElementAt(index);
        }


    }


}