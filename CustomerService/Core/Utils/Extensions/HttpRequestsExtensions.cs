﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Utils.Extensions
{
    public static class HttpRequestsExtensions
    {
        public static async Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request, CancellationToken ct)
        {
            using (ct.Register(request.Abort, useSynchronizationContext: false))
            {
                try
                {
                    var response = await request.GetResponseAsync();
                    return (HttpWebResponse)response;
                }
                catch (WebException ex)
                {
                    if (ct.IsCancellationRequested)
                    {
                        throw new OperationCanceledException(ex.Message, ex, ct);
                    }

                    throw;
                }
            }
        }
    }
}