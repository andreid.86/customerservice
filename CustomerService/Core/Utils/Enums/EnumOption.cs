﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Core.Utils.Enums
{

    public sealed class EnumOption<T> where T : struct, IConvertible
    {
        public string Key { get; set; }
        public int Value { get; set; }

        public EnumOption()
        {

        }

        public EnumOption(T value)
        {
            Key = value.ToString();
            Value = value.ToInt32(null);
        }
    }

    public class EnumList<T> : List<EnumOption<T>> where T : struct, IConvertible
    {
        public EnumList()
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>().ToList().ConvertAll(t => new EnumOption<T>()
            {
                Key = t.ToString(CultureInfo.InvariantCulture),
                Value = t.ToInt32(null),
            });

            values.ForEach(this.Add);
        }


    }
}