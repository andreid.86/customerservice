﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Globalization;
using System.Web.Mvc;
using Formatting = Newtonsoft.Json.Formatting;

namespace Core.Utils.Json
{
    public class JsonNetResult : JsonResult
    {
        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {

            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Culture = new CultureInfo(Core.Constants.Application.CultureInfo),
            TypeNameHandling = TypeNameHandling.None,
            Formatting = Formatting.Indented,
            DateTimeZoneHandling = DateTimeZoneHandling.Utc,
            DateFormatHandling = DateFormatHandling.IsoDateFormat
        };

        public string JsonText => JsonConvert.SerializeObject(Data, JsonSerializerSettings);

        public JsonNetResult()
        {
            
        }

        public JsonNetResult(object data) : this()
        {
            Data = data;
        }

        public JsonNetResult(object data, JsonRequestBehavior jsonRequestBehavior) : this()
        {
            Data = data;
            JsonRequestBehavior = jsonRequestBehavior;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentException(message: "Invalid Context");

            var response = context.HttpContext.Response;
            
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data == null)
                return;

            response.Write(JsonText);
        }
    }
}
