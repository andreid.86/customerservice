﻿namespace Core
{
    public static class Messages
    {
        public static class Errors
        {
            public const string IncorrectUserType = "Incorrect user type";
            public const string CreateRequestError = "Request creation error";
            public const string AssignRequestError = "Error while setting the request assignee";
            public const string UpdateApiKeyError = "Could not update Api Key";
            public const string SuchTimeOfUserCannotBeDeletedError = "Users of this type cannot be deleted";
            public const string UpdateUserStatusError = "Could not update user status";
            public const string UpdateRequestStatusError = "Could not update the request status";
            public const string DeleteRequestError = "Could not delete the request";
            public const string DeleteProcessingRequestError = "The request is being processed, cannot be deleted";
            public const string Unauthorized = "Unauthorized";
            public const string CancelNotNewRequestError = "The request is in progress or has already been completed. It is impossible to cancel";
            public const string InvalidLoginMessage = "Invalid logi attempt. Check your credentials";
        }
    }
}