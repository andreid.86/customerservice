﻿using System;
using DataAccess;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace CustomerService.Web
{
    public sealed partial class Startup
    {
        // For more information on configuring authentication, please visit https://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDataContext.Create);
            app.CreatePerOwinContext<IdentityConfig.ApplicationUserManager>(IdentityConfig.ApplicationUserManager.Create);
            app.CreatePerOwinContext<IdentityConfig.ApplicationSignInManager>(IdentityConfig.ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = ctx =>
                    {
                        var response = ctx.Response;
                        if (!IsApiResponse(ctx.Response))
                        {
                            response.Redirect(ctx.RedirectUri);
                        }
                    },
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<IdentityConfig.ApplicationUserManager, IdentityModels.ApplicationUser, int>(
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentityCallback: (manager, user) => user.GenerateUserIdentityAsync(manager),
                        getUserIdCallback: GrabUserId)
                }
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);


        }

        public static int GrabUserId(System.Security.Claims.ClaimsIdentity claim)
        {
            int id;

            var parsed = int.TryParse(claim.GetUserId(), out id);

            if (!parsed)
            {
                return 0;
            }

            return id;
        }

        private static bool IsApiResponse(IOwinResponse response)
        {
            var responseHeader = response.Headers;

            if (responseHeader == null)
                return false;

            if (!responseHeader.ContainsKey("Suppress-Redirect"))
                return false;

            if (!bool.TryParse(responseHeader["Suppress-Redirect"], out bool suppressRedirect))
                return false;

            return suppressRedirect;
        }
    }
}