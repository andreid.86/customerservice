﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using AutoMapper;
using DataAccess;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Services.AutoMapper;
using Services.Implementations.QueueService;
using Services.Implementations.RequestHandlers;
using Services.Implementations.RequestService;
using Services.Implementations.UserService;
using Services.Interfaces.QueueService;
using Services.Interfaces.RequestService;
using Services.Interfaces.UserService;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Services.Implementations;
using Services.Implementations.FinalizerService;
using Services.Implementations.QueueWatcher;
using Services.Implementations.ReportService;
using Services.Interfaces;
using Services.Interfaces.Finalizer;
using Services.Interfaces.QueueWatcher;
using Services.Interfaces.Reports;

namespace CustomerService.Web
{
    public static class AutofacConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();

            var config = GlobalConfiguration.Configuration;

            builder.RegisterModelBinders();
            builder.RegisterModelBinderProvider();

            // Register MVC Controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterControllers(Assembly.GetExecutingAssembly());

            // Register Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterWebApiModelBinderProvider();

            // Register services
            builder.RegisterType<ReportService>().As<IReportService>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<UserFactory>().As<IUserFactory>().InstancePerLifetimeScope();
            builder.RegisterType<RequestService>().As<IRequestService>().InstancePerLifetimeScope();
            builder.RegisterType<RequestManager>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<AssigneeSetter>().As<IAssigneeSetter>().InstancePerLifetimeScope();
            builder.RegisterType<QueueService>().As<IQueueService>().SingleInstance();

            builder.RegisterType<FinalizerStrategy>().As<IFinalizerStrategy>().InstancePerLifetimeScope();
            builder.RegisterType<TimeBasedGetRequestsForCloseStrategy>().As<IGetRequestsForClosingStrategy>().InstancePerLifetimeScope();
            builder.RegisterType<AssigneeSetter>().As<IAssigneeSetter>().InstancePerLifetimeScope();

            // Register Request Handlers
            builder.RegisterType<SupportHandler>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ManagerHandler>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<ExecutiveHandler>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<QueueHandler>().AsSelf().InstancePerLifetimeScope();

            // Register singleton workers
            builder.RegisterType<TimeBaseQueueWatcher>().As<IQueueWatcher>().SingleInstance().AutoActivate();
            builder.RegisterType<FinalizerService>().As<IFinalizerService>().SingleInstance().AutoActivate();

            // Register Db Context
            builder.RegisterType<ApplicationDataContext>().AsSelf().InstancePerLifetimeScope();

            // Register Identity
            builder.RegisterType<IdentityConfig.ApplicationUserManager>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<IdentityConfig.ApplicationRoleManager>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<IdentityConfig.ApplicationSignInManager>().AsSelf().InstancePerLifetimeScope();
            builder.Register(c => new UserStore<IdentityModels.ApplicationUser, IdentityModels.Role, int, IdentityModels.UserLogin, IdentityModels.UserRole,IdentityModels.UserClaim>(c.Resolve<ApplicationDataContext>())).AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.Register(c => new RoleStore<IdentityModels.Role, int, IdentityModels.UserRole>(c.Resolve<ApplicationDataContext>())).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).As<IAuthenticationManager>();
            builder.Register(c => new IdentityFactoryOptions<IdentityConfig.ApplicationUserManager>
            {
                DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Application​")
            });


            // Creating an Instance for the Mapper
            builder.RegisterInstance(new AutoMapperConfiguration().Configure()).As<IMapper>();

            //builder.RegisterBuildCallback(c => c.Resolve<TimeBaseQueueWatcher>());

            var container = builder.Build();

            // Set the Web API dependency resolver 
            var webApiResolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;

            // Set the MVC dependency resolver 
            var mvcResolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(mvcResolver);

        }
    }
}