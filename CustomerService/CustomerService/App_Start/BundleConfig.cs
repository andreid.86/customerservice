﻿using System.Web.Optimization;
using Core;

namespace CustomerService.Web
{
    public sealed class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterJavaScriptBundles(bundles);
            RegisterCssBundles(bundles);
        }

        private static void RegisterJavaScriptBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.JQueryBundleName}").Include(
                "~/Scripts/Libs/jquery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.JQueryValBundleName}").Include(
                "~/Scripts/Libs/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.ModernizrBundleName}").Include(
                "~/Scripts/Libs/modernizr/modernizr-*"));

            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.BootstrapJsBundleName}").Include(
                "~/Scripts/Libs/bootstrap/bootstrap.js"));

            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.AngularJsBundleName}").Include(
                "~/Scripts/Libs/angular-js/angular.min.js"));


            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.AngularUi}").Include(
                "~/Scripts/Libs/angular-ui/ui-bootstrap.js"));

            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.Toastr}").Include(
                "~/Scripts/Libs/toastr/angular-toastr.tpls.min.js"));


            bundles.Add(new ScriptBundle($"~/bundles/{Constants.WebNames.MainAppJsBundleName}").Include(
                "~/Scripts/App/CustomerServiceApp.js",
                "~/Scripts/App/Stats/StatsController.js",
                "~/Scripts/App/Stats/StatsModel.js",
                "~/Scripts/App/Stats/StatsProxy.js",
                "~/Scripts/App/Users/UsersController.js",
                "~/Scripts/App/Users/UsersModel.js",
                "~/Scripts/App/Users/UsersProxy.js",
                "~/Scripts/App/Core/Proxy.js"));


        }

        private static void RegisterCssBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle($"~/Content/{Constants.WebNames.CssBundleName}").Include(
                "~/Content/css/bootstrap/bootstrap.css",
                "~/Content/css/site.css",
                "~/Content/css/angular-toastr.min.css").
                Include("~/Content/css/font-awesome.css", new CssRewriteUrlTransform()));
        }

    }
}
