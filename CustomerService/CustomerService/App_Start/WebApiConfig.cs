﻿using Core.Utils.Json;
using CustomerService.Web.Helpers.Authorization.Attributes;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CustomerService.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SupportedMediaTypes
                .Add(new MediaTypeHeaderValue("text/html"));
            config.Formatters.JsonFormatter.SerializerSettings = JsonNetResult.JsonSerializerSettings;
            config.MessageHandlers.Add(new SuppressRedirectHandler());

        }
    }
}
