﻿using System.Web.Mvc;

namespace CustomerService.Web
{
    public sealed class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
