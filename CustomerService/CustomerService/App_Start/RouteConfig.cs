﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace CustomerService.Web
{
    public sealed class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            GlobalConfiguration.Configure(c => c.MapHttpAttributeRoutes());
        }
    }
}
