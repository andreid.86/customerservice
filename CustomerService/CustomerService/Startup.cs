﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CustomerService.Web.Startup))]

namespace CustomerService.Web
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}