﻿using Services.Implementations.AuthService;
using Services.Interfaces.AuthService;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CustomerService.Web.Helpers.Authorization.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public sealed class TokenAuthorize : AuthorizationFilterAttribute
    {
        private readonly IAuthService _authService;


        public TokenAuthorize()
        {
            _authService = new AuthService();
        }


        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var skipAuthorization = SkipAuthorization(actionContext);

            if (skipAuthorization)
            {
                return;
            }

            try
            {
                var auth = actionContext.Request.Headers.Authorization;
                if (auth == null)
                {
                    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);
                    return;
                }
                
                var token = auth.Parameter;

                if (string.IsNullOrWhiteSpace(token))
                {
                    actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);
                    return;
                }

                var currentUserDetails = await _authService.GetUserDetailsAsync(token);

                if (currentUserDetails != null)
                {
                    AuthPrincipal currentAuthPrincipal = new AuthPrincipal(new GenericIdentity("token"),
                        new string[] {Core.Constants.RoleNames.Api}) {UserDetails = currentUserDetails};
                    Thread.CurrentPrincipal = currentAuthPrincipal;
                    HttpContext.Current.User = currentAuthPrincipal;
                }

                
            }
            catch (Exception ex)
            {
                actionContext.Response = actionContext.ControllerContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
        }

        private static bool SkipAuthorization(HttpActionContext actionContext)
        {
            var result = actionContext.ActionDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any() || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<AllowAnonymousAttribute>().Any();
            return result;
        }

    }
}