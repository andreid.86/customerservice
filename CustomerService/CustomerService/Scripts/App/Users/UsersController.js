﻿var app = angular.module("CustomerServiceApp");

app.controller("UsersController",
    [
        "$scope",
        "UsersModel",
        "toastr",
        UsersController,
    ]);

function UsersController($scope, model, toastr) {

    var vm = this;

    // #region view bindings

    vm.users = {};
    vm.newUser = {};

    // #region methods
    vm.createUser = createUser;
    vm.deleteUser = deleteUser;
    vm.changeApiKey = changeApiKey;

    // #endregion

    // #endregion

    // #region init

    vm.viewName = model.viewName;
    vm.users = angular.copy(model.users);
    vm.userTypes = angular.copy(model.userTypes);
    vm.userStatuses = angular.copy(model.userStatuses);
    // #endregion

    // #region functions

    // #region binding functions


    vm.getUserStatus  = function(user) {
        var status = vm.userStatuses.find(s => s.id === user.statusId);
        if (!status)
            return "-";
        return status.name;
    }


    vm.getUserType = function(user) {
        var type = vm.userTypes.find(s => s.id === user.typeId);
        if (!type)
            return "-";
        return type.name;
    }

    function createUser(user, userType) {
        var params = {
            email: user.email,
            password: user.password,
            userTypeId: userType.id
        };

        var data = params,
            onSuccess = function (response) {
                
                var jsonResponse = angular.fromJson(response.data);
                var successMessage = "User created";
                vm.users.push(jsonResponse);

                toastr.success(successMessage);
                vm.newUser = {};

            },
            onError = function (response) {
                var errorMessage = response.hasOwnProperty("statusText") ? response.statusText : "Error while creating user";
                toastr.error(errorMessage);
            };
        model.createNewUser(data, onSuccess, onError);
    }

    function deleteUser(user) {

        var userId = user.id;

        var params = {
            userId: userId,
        };

        var data = params,
            onSuccess = function (response) {

                var successMessage = response.statusText;
                
                var deletedIdx = vm.users.map(u => u.id).indexOf(userId);
                vm.users.splice(deletedIdx, 1);

                toastr.success(successMessage);

            },
            onError = function (response) {
                var errorMessage = response.hasOwnProperty("statusText") ? response.statusText : "Error while creating user";
                toastr.error(errorMessage);
            };

        model.deleteUser(data, onSuccess, onError);
    }


    function changeApiKey(user) {

        var userId = user.id;

        var params = {
            userId: userId,
        };

        var data = params,
            onSuccess = function (response) {

                var successMessage = "Api Key Successfully updated";
                debugger;
                var updatedIdx = vm.users.map(u => u.id).indexOf(userId);
                vm.users[updatedIdx].apiKey = response.data;

                toastr.success(successMessage);

            },
            onError = function (response) {
                var errorMessage = response.hasOwnProperty("statusText") ? response.statusText : "Error while creating user";

                toastr.error(errorMessage);
            };

        model.changeApiKey(data, onSuccess, onError);
    }

    // #endregion

    // #region helpers


    // #endregion



    // #endregion

    console.log(vm);
};







