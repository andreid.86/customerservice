﻿var app = angular.module("CustomerServiceApp");

app.factory("UsersModel",
    [
        "serverModel",
        "UsersProxy",
        UsersModel
    ]);

function UsersModel(serverModel, proxy) {

    var model = angular.copy(serverModel);

    model.createNewUser = function(data, onSuccess, oError) {
        proxy.createNewUser(data, onSuccess, oError);
    }

    model.deleteUser = function (data, onSuccess, oError) {
        proxy.deleteUser(data, onSuccess, oError);
    }

    model.changeApiKey = function (data, onSuccess, oError) {
        proxy.changeApiKey(data, onSuccess, oError);
    }

    return model;
}