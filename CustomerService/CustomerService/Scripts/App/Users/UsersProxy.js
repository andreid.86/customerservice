﻿var app = angular.module("CustomerServiceApp");

app.factory("UsersProxy",
    [
        "Proxy",
        UsersProxy
    ]);

function UsersProxy(baseProxy) {

    var api = {};

    var routePrefix = "users";

    api.createNewUser = function(data, onSuccess, onError) {
        baseProxy.post(`/${routePrefix}/create`, data, onSuccess, onError);
    }

    api.deleteUser = function (data, onSuccess, onError) {
        baseProxy.post(`/${routePrefix}/delete`, data, onSuccess, onError);
    }

    api.changeApiKey = function (data, onSuccess, onError) {
        baseProxy.post(`/${routePrefix}/changeApiKey`, data, onSuccess, onError);
    }
    return api;
}

