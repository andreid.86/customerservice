﻿app = angular.module("CustomerServiceApp");

app.factory("Proxy",
    [
        "$rootScope",
        "$http",
        Proxy
    ]);

function Proxy($rootScope, $http) {

    var api = {
        requestConfig: {
            headers: {
            }
        }
    },
        finnaly = function () {
    };

    api.post = function (url, data, onSuccess, onError, bar) {

        $rootScope.$emit('onRequestStart');
        
        var promise = $http.post(url, data, this.requestConfig);

        if (angular.isFunction(onSuccess) && angular.isFunction(onError))
            promise = promise.then(onSuccess, onError);

        promise.finally(finnaly);
        
    };


    api.get = function (url, onSuccess, onError, data) {

        var promise = $http({
            url: url,
            method: "GET",
            params: data
        });

        if (angular.isFunction(onSuccess) && angular.isFunction(onError))
            promise = promise.then(onSuccess, onError);

        return promise.finally(finnaly);

    };

    return api;
}
