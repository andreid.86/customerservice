﻿var app = angular.module("CustomerServiceApp");

app.factory("StatsProxy",
    [
        "Proxy",
        StatsProxy
    ]);

function StatsProxy(baseProxy) {

    var api = {};

    var routePrefix = "stats";

    api.updateData = function(data, onSuccess, onError) {
        baseProxy.get(`/${routePrefix}/update`, onSuccess, onError, data);
    }
    return api;
}

