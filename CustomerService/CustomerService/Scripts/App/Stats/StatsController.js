﻿var app = angular.module("CustomerServiceApp");

app.controller("StatsController",
    [
        "$scope",
        "StatsModel",
        "toastr",
        "$interval",
        StatsController,
    ]);

function StatsController($scope, model, toastr, $interval) {

    var vm = this;
    var interval;
    // #region view bindings

    vm.requests = {};
    vm.users = {};
    vm.queueLength = null;

    // #region methods
    vm.updateData = updateData;
    vm.toggleAutoUpdate = toggleAutoUpdate;
    // #endregion

    // #endregion

    // #region init
    vm.viewName = model.viewName;
    vm.requests = angular.copy(model.requests.rows);
    vm.requestsTableName = angular.copy(model.requests.reportName);
    vm.users = angular.copy(model.users.rows);
    vm.queueLength = model.queueLength;
    // #endregion

    // #region functions

    // #region binding functions


    function updateData(silent) {

        var data = {},
            onSuccess = function (response) {
                var jsonResponse = angular.fromJson(response.data);
                vm.requests = jsonResponse.requests.rows;
                vm.users = jsonResponse.users.rows;
                vm.queueLength = jsonResponse.queueLength;
                var successMessage = "Data successfully updated";
                if (!silent) {
                    toastr.success(successMessage);
                }

            },
            onError = function (response) {
                var errorMessage = response.hasOwnProperty("statusText") ? response.statusText : "Error while updating data";
                if (!silent) {
                    toastr.error(errorMessage);
                }
            };
        model.updateData(data, onSuccess, onError);
    }

    function toggleAutoUpdate(autoUpdate) {
        if (autoUpdate === true) {
            interval = $interval(updateData, 10000);
        } else {
            if (angular.isDefined(interval)) {
                $interval.cancel(interval);
                interval = undefined;
            }
        }
    }

    // #endregion

    // #region helpers


    // #endregion



    // #endregion

    console.log(vm);
};







