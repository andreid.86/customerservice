﻿var app = angular.module("CustomerServiceApp");

app.factory("StatsModel",
    [
        "serverModel",
        "StatsProxy",
        StatsModel
    ]);

function StatsModel(serverModel, proxy) {

    var model = angular.copy(serverModel);

    model.updateData = function(data, onSuccess, oError) {
        proxy.updateData(data, onSuccess, oError);
    }
    return model;
}