﻿using AutoMapper;
using Core.Enums;
using CustomerService.Web.Helpers.Authorization.Attributes;
using Services.Dtos;
using Services.Implementations.AuthService;
using Services.Implementations.RequestHandlers;
using Services.Interfaces.RequestService;
using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace CustomerService.Web.Controllers.Api
{
    [TokenAuthorize]
    [RoutePrefix("api/requests")]
    public class RequestsController : ApiController
    {
        private readonly RequestManager _requestManager;
        private readonly IRequestService _requestService;
        private readonly IMapper _mapper;
        public RequestsController(RequestManager requestManager, IRequestService requestService, IMapper mapper)
        {
            _requestManager = requestManager;
            _requestService = requestService;
            _mapper = mapper;
        }

        [Route("")]
        [TokenAuthorize]
        [HttpPost]
        public HttpResponseMessage CreateRequest(RequestForCreateDto requestForCreateDto)
        {

            try
            {
                if (!(HttpContext.Current.User is AuthPrincipal currentPrincipal))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);

                var user = currentPrincipal.UserDetails;

                var request = _requestService.CreateRequest(requestForCreateDto, user.Id);

                _requestManager.ProcessNewRequest(request);

                var requestForReturn = _mapper.Map<RequestForReturnDto>(request);

                return Request.CreateResponse(HttpStatusCode.OK, requestForReturn);
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }

        }

        [HttpDelete]
        [Route("{id:int}")]
        public HttpResponseMessage CancelRequest(int id)
        {
            try
            {
                if (!(HttpContext.Current.User is AuthPrincipal currentPrincipal))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);

                var currentUserId = currentPrincipal.UserDetails.Id;

                var request = _requestService.GetRequest(id);

                if (request.CreatorId != currentUserId)
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);

                if (request.StatusId != (int) RequestStatus.New)
                    return Request.CreateResponse(HttpStatusCode.BadRequest, Core.Messages.Errors.CancelNotNewRequestError);

                _requestService.DeleteRequest(request.Id);

                return Request.CreateResponse(HttpStatusCode.OK);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }
        }
        
        [TokenAuthorize]
        [HttpGet]
        [Route("{id:int}")]
        public HttpResponseMessage GetRequestStatus(int id)
        {
            try
            {
                if (!(HttpContext.Current.User is AuthPrincipal currentPrincipal))
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);

                var currentUserId = currentPrincipal.UserDetails.Id;

                var request = _requestService.GetRequest(id);

                if (request.CreatorId != currentUserId)
                    return Request.CreateResponse(HttpStatusCode.Unauthorized, Core.Messages.Errors.Unauthorized);

                var requestForReturn = _mapper.Map<RequestForReturnDto>(request);

                return Request.CreateResponse(HttpStatusCode.OK, requestForReturn);
            }

            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, e.Message);
            }

        }

    }
}
