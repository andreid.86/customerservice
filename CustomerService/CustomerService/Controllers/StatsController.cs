﻿using Core.Enums;
using Core.Utils.Enums;
using Core.Utils.Json;
using CustomerService.Web.ViewModels.Stats;
using Services.Implementations.ReportService.Reports.RequestsReport;
using Services.Implementations.ReportService.Reports.UsersReport;
using Services.Interfaces.QueueService;
using Services.Interfaces.Reports;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CustomerService.Web.Controllers
{
    [Authorize(Roles = Core.Constants.RoleNames.Administrator)]
    [RoutePrefix("stats")]
    public class StatsController : Controller
    {
        private readonly IReportService _reportService;
        private readonly IQueueService _queueService;

        public StatsController(IReportService reportService, IQueueService queueService)
        {
            _reportService = reportService;
            _queueService = queueService;
        }

        [Route("")]
        [Route("Index")]
        public async Task<ActionResult> Index()
        {

            // Compromise approach
            // It is necessary to take filters from the front
            // But since there is no such requirement, filters are set directly in the code

            var requestReportFilter = new RequestReportFilter()
            {
                Since = (DateTime?)null,
                Until = (DateTime?)null,
                Count = 20,
            };

            var usersReportFilter = new UsersReportFilter()
            {
                UserTypes = new EnumList<UserType>()
                    .Where(t => t.Value != (int)UserType.Customer)
                    .Select(t => t.Value).ToList(),
            };

            var viewModel = new StatsViewModel()
            {
                ViewName = "Customer Service Statistics",
                Requests = await _reportService.GetRequestsReportAsync(requestReportFilter),
                Users = await _reportService.GetUsersReportAsync(usersReportFilter),
                QueueLength = _queueService.Length
            };

            return View("~/Views/Stats/Index.cshtml", viewModel);
        }

        [Route("update")]
        [HttpGet]
        public async Task<ActionResult> UpdateIndex()
        {
            // Compromise approach
            // It is necessary to take filters from the front
            // But since there is no such requirement, filters are set directly in the code

            var requestReportFilter = new RequestReportFilter()
            {
                Since = (DateTime?)null,
                Until = (DateTime?)null,
                Count = 20,
            };

            var usersReportFilter = new UsersReportFilter()
            {
                UserTypes = new EnumList<UserType>()
                    .Where(t => t.Value != (int)UserType.Customer)
                    .Select(t => t.Value).ToList(),
            };

            var viewModel = new StatsViewModel()
            {
                ViewName = "Customer Service Statistics",
                Requests = await _reportService.GetRequestsReportAsync(requestReportFilter),
                Users = await _reportService.GetUsersReportAsync(usersReportFilter),
                QueueLength = _queueService.Length
            };

            var result = new JsonNetResult(viewModel, JsonRequestBehavior.AllowGet);

            return result;
        }
    }
}