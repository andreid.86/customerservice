﻿using Core.Utils.Json;
using CustomerService.Web.ViewModels.Users;
using Services.Dtos;
using Services.Interfaces.UserService;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using IMapper = AutoMapper.IMapper;

namespace CustomerService.Web.Controllers
{
    [RoutePrefix("users")]
    [Authorize(Roles = Core.Constants.RoleNames.Administrator)]
    public class UsersController : Controller
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly int _adminRoleId;
        private readonly int _apiRoleId;

        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;

            var roles = _userService.GetRoles();
            _adminRoleId = roles.FirstOrDefault(r => r.Name == Core.Constants.RoleNames.Administrator)?.Id ?? 0;
            _apiRoleId = roles.FirstOrDefault(r => r.Name == Core.Constants.RoleNames.Api)?.Id ?? 0;

        }

        [Route("")]
        [Route("Index")]
        public ActionResult Index()
        {
            var users = _userService.GetUsers().ToList();

            var userList = new Collection<UserForListDto>();

            users.ForEach(user =>
            {
                var userDto = _mapper.Map<UserForListDto>(user);
                userDto.IsAdmin = user.Roles.Select(r => r.RoleId).Contains(_adminRoleId);
                userDto.IsApiUser = user.Roles.Select(r => r.RoleId).Contains(_apiRoleId);

                userList.Add(userDto);
            });


            var viewModel = new UsersViewModel
            {
                ViewName = "Users Management",
                UserTypes = _userService.GetUserTypes(),
                UserStatuses = _userService.GetUserStatuses(),
                Users = userList
            };

            return View("~/Views/Users/Index.cshtml", viewModel);
        }

        [HttpPost]
        [Route("create")]
        public ActionResult Create(UserForCreateDto request)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(request.Email) || string.IsNullOrWhiteSpace(request.Password))
                    throw new ArgumentException("User or password can not be empty");

                var createdUser = _userService.CreateUser(request);
                var userForReturn = _mapper.Map<UserForListDto>(createdUser);
                userForReturn.IsAdmin = createdUser.Roles.Select(r => r.RoleId).Contains(_adminRoleId);
                userForReturn.IsApiUser = createdUser.Roles.Select(r => r.RoleId).Contains(_apiRoleId);


                var result = new JsonNetResult(userForReturn);
                return result;

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }

        }

        [HttpPost]
        [Route("delete")]
        public ActionResult Delete(int userId)
        {
            try
            {
                return _userService.DeleteUser(userId) ? new HttpStatusCodeResult(200, "User successfully deleted") : new HttpStatusCodeResult(500, "Could not delete user");
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }

        }

        [HttpPost]
        [Route("changeApiKey")]
        public ActionResult ChangeApiKey(int userId)
        {
            try
            {
                var result = _userService.ChangeApiKey(userId);
                return  new JsonNetResult(result);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(500, ex.Message);
            }

        }
    }
}