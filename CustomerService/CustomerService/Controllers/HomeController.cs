﻿using System.Web.Mvc;

namespace CustomerService.Web.Controllers
{
    [Authorize(Roles = Core.Constants.RoleNames.Administrator)]
    public class HomeController : Controller
    {


        public HomeController()
        {
        }

        [Route("")]
        public ActionResult Index()
        {

            return RedirectToAction("Index", "Stats");
        }

    }
}