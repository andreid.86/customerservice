﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CustomerService.Web.ViewModels.Account;
using DataAccess;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace CustomerService.Web.Controllers
{
    [Authorize]
    [RoutePrefix("Account")]
    public class AccountController : Controller
    {
        private readonly IdentityConfig.ApplicationSignInManager _signInManager;

        public AccountController(IdentityConfig.ApplicationSignInManager signInManager)
        {
            _signInManager = signInManager;
        }


        [AllowAnonymous]
        [Route("Login")]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            var viewModel = new AccountViewModels.LoginViewModel();
            
            return View("~/Views/Account/Login.cshtml", viewModel);
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Route("Login")]
        public async Task<ActionResult> Login(AccountViewModels.LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View("~/Views/Account/Login.cshtml", model);
            }

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return RedirectToLoginAction();
                case SignInStatus.RequiresVerification:
                    throw new NotImplementedException();
                case SignInStatus.Failure:
                    ModelState.AddModelError("", Core.Messages.Errors.InvalidLoginMessage);
                    return View(model);
                default:
                    ModelState.AddModelError("", Core.Messages.Errors.InvalidLoginMessage);
                    return View(model);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("LogOff")]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        private ActionResult RedirectToLoginAction()
        {
            return RedirectToAction("Login", "Account");
        }

        #endregion
    }
}