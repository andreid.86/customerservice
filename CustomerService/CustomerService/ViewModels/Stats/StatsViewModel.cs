﻿using Services.Implementations.ReportService.Reports.RequestsReport;
using Services.Implementations.ReportService.Reports.UsersReport;

namespace CustomerService.Web.ViewModels.Stats
{
    public class StatsViewModel : BaseViewModel
    {
        public RequestReport Requests { get; set; }
        public UsersReport Users { get; set; }
        public int QueueLength { get; set; }
    }
}