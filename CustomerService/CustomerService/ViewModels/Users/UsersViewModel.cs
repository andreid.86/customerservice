﻿using System.Collections.Generic;
using DataAccess.BaseModels;
using Services.Dtos;

namespace CustomerService.Web.ViewModels.Users
{
    public class UsersViewModel : BaseViewModel
    {
        public ICollection<UserForListDto> Users { get; set; }
        public ICollection<UserTypeBase> UserTypes { get; set; }
        public ICollection<UserStatusBase> UserStatuses { get; set; }
    }
}