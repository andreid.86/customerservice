﻿using DataAccess;

namespace CustomerService.Web.ViewModels
{
    public class BaseViewModel
    {
        public bool IsAdmin { get; }
        public string UserName { get; }

        public string ViewName { get; set; }

        public BaseViewModel()
        {
            IsAdmin = System.Web.HttpContext.Current.User.IsInRole(Core.Constants.RoleNames.Administrator);
            UserName = IdentityModels.ApplicationUser.GetUserName(System.Web.HttpContext.Current.User.Identity.Name);
        }
    }

    


}