﻿namespace DataAccess.BaseModels
{
    public class UserTypeBase : INameWithId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}