﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Core.Utils.Extensions;

namespace DataAccess.BaseModels
{
    public class RequestBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? AssigneeId { get; set; }
        public long? CreationTimeStamp { get; set; }
        public long? DeletionTimeStamp { get; set; }
        public long? LastStatusChangingTimestamp { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int CreatorId { get; set; }
        public int StatusId { get; set; }

        /// <summary>
        /// Returns age of the current request in seconds
        /// </summary>
        public long GetRequestAge()
        {
            var result = CreationTimeStamp != null ? (DateTime.UtcNow.ToUnixTimestamp() - CreationTimeStamp) : 0;
            return (long) result;
        }
    }
}