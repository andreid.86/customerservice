﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.BaseModels
{
    public interface INameWithId
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        int Id { get; set; }
        [Required]
        [StringLength(255)]
        string Name { get; set; }
    }
}