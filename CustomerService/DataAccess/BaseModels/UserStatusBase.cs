﻿namespace DataAccess.BaseModels
{
    public class UserStatusBase : INameWithId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}