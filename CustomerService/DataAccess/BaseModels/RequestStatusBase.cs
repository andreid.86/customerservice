﻿namespace DataAccess.BaseModels
{
    public class RequestStatusBase : INameWithId
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}