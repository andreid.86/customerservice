﻿using DataAccess.BaseModels;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Entities
{
    public class UserStatusEntity : UserStatusBase
    {
        public ICollection<IdentityModels.ApplicationUser> Users { get; set; }
    }

    public class UserStatusEntityMap : EntityTypeConfiguration<UserStatusEntity>
    {
        public UserStatusEntityMap()
        {
            HasMany(s => s.Users)
                .WithOptional(u => u.Status)
                .HasForeignKey(u => u.StatusId)
                .WillCascadeOnDelete(false);
            ToTable(Core.Constants.TableNames.UserStatusesTableName);
        }
    }
}