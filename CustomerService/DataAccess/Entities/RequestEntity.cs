﻿using Core.Utils.Extensions;
using DataAccess.BaseModels;
using System;
using System.Data.Entity.ModelConfiguration;

namespace DataAccess.Entities
{
    public class RequestEntity : RequestBase
    {
        public RequestStatusEntity Status { get; set; }
        public IdentityModels.ApplicationUser Creator { get; set; }
        public IdentityModels.ApplicationUser Assignee { get; set; }

        public RequestEntity()
        {
            CreationTimeStamp = DateTime.UtcNow.ToUnixTimestamp();
        }
    }

    public class RequestEntityMap : EntityTypeConfiguration<RequestEntity>
    {
        public RequestEntityMap()
        {
            HasKey(k => new { k.CreationTimeStamp, k.Id });
            ToTable(Core.Constants.TableNames.RequestsTableName);

            HasOptional(r => r.Assignee)
                .WithMany(u => u.AssingedRequests)
                .WillCascadeOnDelete(false);

            HasRequired(r => r.Creator)
                .WithMany(u => u.CreatedRequests)
                .WillCascadeOnDelete(false);

        }
    }
}
