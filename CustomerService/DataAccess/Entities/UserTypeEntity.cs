﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using DataAccess.BaseModels;

namespace DataAccess.Entities
{
    public class UserTypeEntity : UserTypeBase
    {
        public ICollection<IdentityModels.ApplicationUser> Users { get; set; }
    }

    public class UserTypeEntityMap : EntityTypeConfiguration<UserTypeEntity>
    {
        public UserTypeEntityMap()
        {
            
            HasMany(s => s.Users)
                .WithRequired(u => u.Type)
                .HasForeignKey(u => u.TypeId)
                .WillCascadeOnDelete(false);
            ToTable(Core.Constants.TableNames.UserTypesTableName);
        }
    }
}