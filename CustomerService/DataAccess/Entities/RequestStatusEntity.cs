﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using DataAccess.BaseModels;

namespace DataAccess.Entities
{
    public class RequestStatusEntity : RequestStatusBase
    {
        public ICollection<RequestEntity> Requests { get; set; }
    }

    public class RequestStatusEntityMap : EntityTypeConfiguration<RequestStatusEntity>
    {
        public RequestStatusEntityMap()
        {
            HasMany(s => s.Requests)
                .WithRequired(r => r.Status)
                .HasForeignKey(s => s.StatusId)
                .WillCascadeOnDelete(false);
            ToTable(Core.Constants.TableNames.RequestStatusesTableName);
        }
    }
}