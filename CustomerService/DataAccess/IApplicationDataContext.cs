﻿using DataAccess.Entities;
using System.Data.Entity;

namespace DataAccess
{
    public interface IApplicationDataContext
    {
        IDbSet<UserStatusEntity> UserStatuses { get; set; }
        IDbSet<UserTypeEntity> UserTypes { get; set; }
        IDbSet<RequestStatusEntity> RequestStatuses { get; set; }
        IDbSet<RequestEntity> Requests { get; set; }
    }
}