﻿using Core;
using DataAccess.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Configuration;
using System.Data.Entity;


namespace DataAccess
{
    public sealed class ApplicationDataContext : IdentityDbContext<IdentityModels.ApplicationUser, IdentityModels.Role, int, IdentityModels.UserLogin, IdentityModels.UserRole, IdentityModels.UserClaim>, IApplicationDataContext
    {


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            #region Identity Model Bindings

            modelBuilder.Entity<IdentityModels.Role>().ToTable(Core.Constants.TableNames.RolesTableName);
            modelBuilder.Entity<IdentityModels.UserRole>().ToTable(Core.Constants.TableNames.UserRolesTableName);
            modelBuilder.Entity<IdentityModels.UserClaim>().ToTable(Core.Constants.TableNames.UserClaimsTableName);
            modelBuilder.Entity<IdentityModels.UserLogin>().ToTable(Core.Constants.TableNames.UserLoginsTableName);
            modelBuilder.Entity<IdentityModels.ApplicationUser>().ToTable(Core.Constants.TableNames.UsersTableName);

            #endregion


            modelBuilder.Configurations.Add(new UserTypeEntityMap());
            modelBuilder.Configurations.Add(new UserStatusEntityMap());
            modelBuilder.Configurations.Add(new RequestStatusEntityMap());
            modelBuilder.Configurations.Add(new RequestEntityMap());

        }

        public ApplicationDataContext() : base(Constants.DataContext.Name)
        {
            Database.CommandTimeout = Constants.DataContext.CommandTimeout;
        }

        static ApplicationDataContext()
        {
            Database.SetInitializer<ApplicationDataContext>(new DbInitializer());
            using (var context = new ApplicationDataContext())
            {
                context.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings[Core.Constants.DataContext.Name].ConnectionString;
                context.Database.Initialize(false);
            }

        }

        public static ApplicationDataContext Create()
        {
            var context = new ApplicationDataContext();
            return context;
        }

        public IDbSet<UserStatusEntity> UserStatuses { get; set; }
        public IDbSet<UserTypeEntity> UserTypes { get; set; }
        public IDbSet<RequestStatusEntity> RequestStatuses { get; set; }
        public IDbSet<RequestEntity> Requests { get; set; }
    }
}