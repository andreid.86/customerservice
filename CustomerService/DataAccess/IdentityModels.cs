﻿using DataAccess.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DataAccess
{
    public sealed class IdentityModels
    {
        public class UserRole : IdentityUserRole<int>
        {
            
        }

        public class UserClaim : IdentityUserClaim<int>
        {
            
        }

        public class UserLogin : IdentityUserLogin<int>
        {
            
        }

        public class Role : IdentityRole<int, UserRole>
        {
            public Role()
            {
                
            }

            public Role(string name)
            {
                Name = name;
            }
        }

        public class UserStore : UserStore<ApplicationUser, Role, int, UserLogin, UserRole, UserClaim>
        {
            public UserStore(DbContext context) : base(context)
            {
                
            }
        }

        public class RoleStore : RoleStore<Role, int, UserRole>
        {
            public RoleStore(DbContext context) : base(context)
            {
                
            }
        }

        public sealed class ApplicationUser : IdentityUser<int, UserLogin, UserRole, UserClaim>
        {
            public int? StatusId { get; set; }
            public UserStatusEntity Status { get; set; }

            public int TypeId { get; set; }
            public UserTypeEntity Type { get; set; }

            public ICollection<RequestEntity> CreatedRequests { get; set; }
            public ICollection<RequestEntity> AssingedRequests { get; set; }

            public string ApiKey { get; set; }
            public long? ApiKeyCreationTimeStamp { get; set; }
            public long CreationTimeStamp { get; set; }
            public long? DeletionTimeStamp { get; set; }
            
            public ApplicationUser()
            {

            }

            public static string GetUserName(string email)
            {
                var username = !string.IsNullOrWhiteSpace(email) ? email.Split('@').ElementAtOrDefault(0) : Core.Constants.Identity.AnonymousUserName;
                return username;
            }

            public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser, int> manager)
            {
                var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
                return userIdentity;
            }
        }

    }
}