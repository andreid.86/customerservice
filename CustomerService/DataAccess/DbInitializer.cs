﻿using Core.Enums;
using Core.Utils;
using Core.Utils.Enums;
using Core.Utils.Extensions;
using DataAccess.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using Constants = Core.Constants;

namespace DataAccess
{
    public sealed class DbInitializer : CreateDatabaseIfNotExists<ApplicationDataContext>
    {
        protected override void Seed(ApplicationDataContext context)
        {
            SeedRoles(context);
            SeedUserTypes(context);
            SeedUserStatuses(context);
            SeedRequestStatuses(context);
            SeedUsers(context);
            base.Seed(context);
        }

        private static void SeedRoles(ApplicationDataContext context)
        {
            var isSeeded = context.Roles.Any(u => u.Name.ToLower() == Constants.RoleNames.Administrator);

            if (isSeeded) return;

            FieldInfo[] fieldInfos =
                typeof(Constants.RoleNames).GetFields(BindingFlags.Public | BindingFlags.Static |
                                                      BindingFlags.FlattenHierarchy);
            var roleNames = fieldInfos.Where(fi => fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(string))
                .Select(r => (string)r.GetRawConstantValue()).ToList();

            var roleManager = new IdentityConfig.ApplicationRoleManager(new IdentityModels.RoleStore(context));

            roleNames.ForEach(roleName =>
            {
                if (!roleManager.RoleExists(roleName))
                {
                    var role = new IdentityModels.Role { Name = roleName };
                    roleManager.Create(role);
                }
            });

        }

        private static void SeedUsers(ApplicationDataContext context)
        {
            const uint numberOfEachTypeOfUser = 11;

            var isSeeded = context.Users.Any();

            if (isSeeded) return;

            foreach (UserType userType in Enum.GetValues(typeof(UserType)))
            {
                SeedUsersByType(userType, numberOfEachTypeOfUser, context);
            }
        }

        private static void SeedUserTypes(ApplicationDataContext context)
        {
            var isSeeded = context.UserTypes.Any(u => u.Id == (int)UserType.Admin);

            if (isSeeded) return;


            foreach (var type in new EnumList<UserType>())
            {
                context.UserTypes.Add(new UserTypeEntity() { Id = type.Value, Name = type.Key });
            }

            context.SaveChanges();
        }

        private static void SeedRequestStatuses(ApplicationDataContext context)
        {
            var isSeeded = context.RequestStatuses.Any(u => u.Id == Constants.RequestStatuses.DefaultRequestStatus);

            if (isSeeded) return;


            foreach (var status in new EnumList<RequestStatus>())
            {
                context.RequestStatuses.Add(new RequestStatusEntity() { Id = status.Value, Name = status.Key });
            }

            context.SaveChanges();
        }

        private static void SeedUserStatuses(ApplicationDataContext context)
        {
            var isSeeded = context.UserStatuses.Any(u => u.Id == Constants.UserStatuses.DefaultUserStatus);

            if (isSeeded) return;

            foreach (var status in new EnumList<UserStatus>())
            {
                context.UserStatuses.Add(new UserStatusEntity() { Id = status.Value, Name = status.Key });
            }

            context.SaveChanges();
        }

        private static void SeedUsersByType(UserType userType, uint count, ApplicationDataContext context)
        {
            const uint maxNumberOfUsers = 10;
            
            uint numberOfUsers = 1;
            
            if ((userType != UserType.Executive) &&
                (userType != UserType.Admin))
            {
                numberOfUsers = (count <= maxNumberOfUsers) ? count : maxNumberOfUsers;
            }
            
            var userTypeId = (int) userType;
            var stringUserType = userType.ToString();

            using (var userManager = new IdentityConfig.ApplicationUserManager(new IdentityModels.UserStore(context)))
            {
                for (var userIdx = 0; userIdx < numberOfUsers; userIdx++)
                {
                    var userPostfix = numberOfUsers > 1 ? string.Concat("_", $"{userIdx:D2}") : string.Empty;
                    var userEmail = string.Concat(stringUserType, userPostfix, '@', Core.Constants.Identity.DefaultUserDomain);

                    var user = new IdentityModels.ApplicationUser
                    {
                        UserName = userEmail,
                        Email = userEmail,
                        EmailConfirmed = true,
                        PhoneNumberConfirmed = true,
                        PasswordHash = userManager.PasswordHasher.HashPassword(Constants.Identity.DefaultUserPassword),
                        LockoutEnabled = true,
                        TypeId = userTypeId,
                        StatusId = (userType == UserType.Support) ? (int?) UserStatus.Free : null,
                        ApiKey = (userType == UserType.Customer) ? Randoms.ApiKey : null,
                        ApiKeyCreationTimeStamp = (userType == UserType.Customer) ? (long?) DateTime.UtcNow.ToUnixTimestamp() :  null,
                    };
                    
                    var creationResult = userManager.Create(user);

                    if (creationResult != IdentityResult.Success) continue;
                    
                    switch (userType)
                    {
                        case UserType.Executive:
                        case UserType.Admin:
                            userManager.AddToRole(user.Id, Constants.RoleNames.Administrator);
                            break;
                        case UserType.Customer:
                            userManager.AddToRole(user.Id, Constants.RoleNames.Api);
                            break;
                    }
                }
            };
        }
    }
}