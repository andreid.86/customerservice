﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace DataAccess
{
    public class IdentityConfig
    {
        public class EmailService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                throw new NotImplementedException();
            }
        }

        public class SmsService : IIdentityMessageService
        {
            public Task SendAsync(IdentityMessage message)
            {
                throw new NotImplementedException();
            }
        }

        
        public class ApplicationRoleManager : RoleManager<IdentityModels.Role, int>
        {
            public ApplicationRoleManager(IRoleStore<IdentityModels.Role, int> store) : base(store)
            {
                
            }

            public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
            {
                return new ApplicationRoleManager(new RoleStore<IdentityModels.Role, int, IdentityModels.UserRole>(context.Get<ApplicationDataContext>()));
            }
        }
        // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
        public class ApplicationUserManager : UserManager<IdentityModels.ApplicationUser, int>
        {
            public ApplicationUserManager(IUserStore<IdentityModels.ApplicationUser, int> store) : base(store)
            {
            }

            public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options,
                IOwinContext context)
            {
                var manager = new ApplicationUserManager(new IdentityModels.UserStore(context.Get<ApplicationDataContext>()));
                // Configure validation logic for usernames
                manager.UserValidator = new UserValidator<IdentityModels.ApplicationUser, int>(manager)
                {
                    AllowOnlyAlphanumericUserNames = Core.Constants.Identity.AllowOnlyAlphanumericUserNames,
                    RequireUniqueEmail = Core.Constants.Identity.RequireUniqueEmail
                };

                // Configure validation logic for passwords
                manager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = Core.Constants.Identity.RequiredPasswordLength,
                    RequireNonLetterOrDigit = Core.Constants.Identity.RequireNonLetterOrDigit,
                    RequireDigit = Core.Constants.Identity.RequireDigit,
                    RequireLowercase = Core.Constants.Identity.RequireLowercase,
                    RequireUppercase = Core.Constants.Identity.RequireUppercase,
                };

                // Configure user lockout defaults
                manager.UserLockoutEnabledByDefault = Core.Constants.Identity.UserLockoutEnabledByDefault;
                manager.DefaultAccountLockoutTimeSpan =
                    TimeSpan.FromMinutes(Core.Constants.Identity.DefaultAccountLockoutTimeSpanMinutes);
                manager.MaxFailedAccessAttemptsBeforeLockout =
                    Core.Constants.Identity.MaxFailedAccessAttemptsBeforeLockout;

                // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
                // You can write your own provider and plug it in here.

                manager.EmailService = new EmailService();
                manager.SmsService = new SmsService();

                var dataProtectionProvider = options.DataProtectionProvider;

                if (dataProtectionProvider != null)
                {
                    manager.UserTokenProvider =
                        new DataProtectorTokenProvider<IdentityModels.ApplicationUser, int>(
                            dataProtectionProvider.Create("ASP.NET Identity"));
                }

                return manager;
            }
        }

        public class ApplicationSignInManager : SignInManager<IdentityModels.ApplicationUser, int>
        {
            public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
                : base(userManager, authenticationManager)
            {
            }

            public override Task<ClaimsIdentity> CreateUserIdentityAsync(IdentityModels.ApplicationUser user)
            {
                return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
            }

            public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
            {
                return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
            }
        }
    }
}