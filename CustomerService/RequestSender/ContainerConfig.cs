﻿using Autofac;
using Services.Implementations.Loggers;
using Services.Interfaces;

namespace RequestSender
{
    public sealed class ContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Runner>().AsSelf();

            builder.RegisterType<SendRequest>().As<ISendRequest>().InstancePerLifetimeScope();

            builder.RegisterType<DbTokenGetter>().As<ITokensGetter>().InstancePerLifetimeScope();

            builder.RegisterType<ConsoleLoggerService>().As<ILoggerService>().SingleInstance();

            return builder.Build();
        }
    }
}