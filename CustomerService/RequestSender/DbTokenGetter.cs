﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace RequestSender
{
    public sealed class DbTokenGetter : ITokensGetter
    {
        protected static string ConnectionString = ConfigurationManager.ConnectionStrings[Core.Constants.DataContext.Name].ToString();
        protected static int CommandTimeout = Core.Constants.DataContext.CommandTimeout;

        private readonly string _sqlQueryForGetTokens = $"SELECT ApiKey " +
                                                             $"FROM {Core.Constants.TableNames.UsersTableName} " +
                                                             $"WHERE " +
                                                             $"(ApiKey IS NOT NULL) " +
                                                             $"AND (DeletionTimeStamp IS NULL) " +
                                                             $"AND (LockOutEndDateUtc IS NULL OR LockOutEndDateUtc < @now)";

        public async Task<ICollection<string>> GetTokensAsync()
        {
            var currentDateTime = DateTime.UtcNow;

            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var query = await conn
                        .QueryAsync<string>(_sqlQueryForGetTokens, new {@now = currentDateTime},
                            commandTimeout: CommandTimeout);

                    var tokens = query.ToList();

                    return tokens;

                }
                catch
                {
                    throw new Exception("Unable to get one authorization token");
                }
            }


        }
    }
}