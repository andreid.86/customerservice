﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace RequestSender
{
    public interface ITokensGetter
    {
        Task<ICollection<string>> GetTokensAsync();
    }
}