﻿using Autofac;
using CommandLine;
using Services.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RequestSender
{
    internal sealed class Program
    {
        private static async Task Main(string[] args)
        {
            await Parser.Default.ParseArguments<Options>(args)
                .MapResult(async opts =>
                {
                    await StartApp(opts.LowerBorderTime, opts.UpperBorderTime, opts.RequestsNumber, opts.Url);
                }, errors => Task.FromResult(0));
        }

        private static async Task StartApp(uint lowerTimeBorder, uint upperTimeBorder, uint? count, string endPoint)
        {
            var container = ContainerConfig.Configure();

            using (var scope = container.BeginLifetimeScope())
            {
                var runner = scope.Resolve<Runner>();
                var logger = scope.Resolve<ILoggerService>();

                if (upperTimeBorder < lowerTimeBorder)
                {
                    logger.LogError("Upper border of a request execution time range can not be more than lower. Borders are swapped");
                }

                logger.LogInfo("Application has started. Press Ctrl-C to exit");

                try
                {
                    var cancellationTokenSource = new CancellationTokenSource();
                    var cancellationToken = cancellationTokenSource.Token;
                    cancellationToken.ThrowIfCancellationRequested();

                    Console.CancelKeyPress += (sender, eventArgs) =>
                    {
                        cancellationTokenSource.Cancel();
                        eventArgs.Cancel = true;
                    };

                    await runner.StartAsync(lowerTimeBorder, upperTimeBorder, count, endPoint, cancellationToken);
                }

                catch (Exception ex)
                {

                    if (ex is TaskCanceledException)
                        logger.LogInfo("Application stop requested. Stopping...");
                    else
                    {
                        logger.LogException(ex);
                    }
                }
            }
        }
    }
}