﻿using System.Net;
using Services.Dtos;

namespace RequestSender
{
    public class RequestResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public RequestForReturnDto ResponseData { get; set; }
    }
}