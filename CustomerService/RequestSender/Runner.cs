﻿using Core.Utils.Extensions;
using Services.Dtos;
using Services.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace RequestSender
{
    public sealed class Runner
    {
        private readonly ISendRequest _sendRequest;
        private readonly ITokensGetter _tokensGetter;
        private readonly ILoggerService _logger;

        private readonly string _newRequestBodyString = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm} Request Body";
        private readonly string _newRequestSubjectString = $"{DateTime.UtcNow:yyyy-MM-dd HH:mm} Request Subject";

        public Runner(ISendRequest sendRequest, ITokensGetter tokensGetter, ILoggerService logger)
        {
            _sendRequest = sendRequest;
            _tokensGetter = tokensGetter;
            _logger = logger;
        }

        public async Task StartAsync(uint lowerTimeBorder, uint upperTimeBorder, uint? count, string endPoint, CancellationToken cancellationToken)
        {
            var randomIntervalSeconds = Core.Utils.Randoms.RandomBetween((int)lowerTimeBorder, (int)upperTimeBorder);

            _logger.LogInfo($"Random interval between requests: {randomIntervalSeconds}");


            var tokens = await _tokensGetter.GetTokensAsync();

            if (tokens.IsNullOrEmpty())
                throw new Exception("Could not get authentication data to send requests");

            _logger.LogInfo($"Number of requests to be sent: {(count != null ? count.ToString() : "until the user interrupts")}");

            var requestIdx = 0;
            do
            {
                var request = new RequestForCreateDto()
                {
                    Body = _newRequestBodyString,
                    Subject = _newRequestSubjectString,
                };

                var randomToken = tokens.TakeRandom();

                await _sendRequest.SendAsync(request, endPoint, randomToken, cancellationToken);

                _logger.LogInfo($"Request has been sent. Request number: {requestIdx + 1}");

                await Task.Delay(TimeSpan.FromSeconds(randomIntervalSeconds), cancellationToken);

                requestIdx++;

            } while (count == null || (requestIdx < count));
        }
    }
}