﻿using Newtonsoft.Json;
using Services.Dtos;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Utils.Extensions;

namespace RequestSender
{
    public sealed class SendRequest : ISendRequest
    {
        public async Task<RequestResponse> SendAsync(RequestForCreateDto request, string apiEndpoint, string token, CancellationToken cancellationToken)
        {
            var jsonSerializerSettings = Core.Utils.Json.JsonNetResult.JsonSerializerSettings;

            var jsonString = JsonConvert.SerializeObject(request, Formatting.Indented, jsonSerializerSettings);

            var byteArray = Encoding.UTF8.GetBytes(jsonString);

            var webRequest = (HttpWebRequest)WebRequest.Create(apiEndpoint);
                webRequest.Method = "post";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = byteArray.Length;
                webRequest.PreAuthenticate = true;
                webRequest.Headers.Add("Authorization", "Bearer " + token);


            using (var postStream = await webRequest.GetRequestStreamAsync())
            {
                await postStream.WriteAsync(byteArray, 0, byteArray.Length, cancellationToken);
            }


            var webResponse = (HttpWebResponse)await webRequest.GetResponseAsync(cancellationToken);

            using (var reader = new StreamReader(webResponse.GetResponseStream() ?? throw new InvalidOperationException()))
            {
                var data = await reader.ReadToEndAsync();

                var requestResponse = new RequestResponse()
                {
                    ResponseData = JsonConvert.DeserializeObject<RequestForReturnDto>(data),
                    StatusCode = ((HttpWebResponse)webResponse).StatusCode
                };

                return requestResponse;
            }



        }
    }
}