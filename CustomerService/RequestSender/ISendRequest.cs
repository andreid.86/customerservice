﻿using System.Threading;
using System.Threading.Tasks;
using Services.Dtos;

namespace RequestSender
{
    public interface ISendRequest
    {
        Task<RequestResponse> SendAsync(RequestForCreateDto request, string apiEndpoint, string token, CancellationToken cancellationToken);
    }
}