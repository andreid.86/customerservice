﻿using CommandLine;

namespace RequestSender
{
    public sealed class Options
    {
        private const string RequestBasePath = "/api/requests";

        [Option('h', "hostname", Required = true, HelpText = "Hostname or IP address to send api request")]
        public string Host { get; set; }

        [Option('p', "port", Required = true, HelpText = "Server with api endpoint port number")]
        public uint Port { get; set; }

        [Option('l', "lower-border-time", Required = true, HelpText = "Lower border of a random time range between requests")]
        public uint LowerBorderTime { get; set; }

        [Option('u', "upper-border-time", Required = true, HelpText = "Upper border of a random time range between requests")]
        public uint UpperBorderTime { get; set; }

        [Option('n', "requests-number", Required = false, Default = null, HelpText = "Number of requests to generate")]
        public uint? RequestsNumber { get; set; }

        [Option('s', "ssl", Required = false, Default = false, HelpText = "Use ssl to send request")]
        public bool UseSecure { get; set; }

        public string Url => (UseSecure ? "https://" : "http://") + Host + ":" + Port + RequestBasePath;
    }
}