﻿namespace Services.Dtos
{
    public class RequestForCreateDto
    {
        public string Subject { get; set; }
        public string Body { get; set; }

    }
}