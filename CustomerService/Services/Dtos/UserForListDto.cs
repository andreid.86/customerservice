﻿namespace Services.Dtos
{
    public class UserForListDto
    {
        public uint Id { get; set; }
        public uint TypeId { get; set; }
        public uint? StatusId { get; set; }
        public string Email { get; set; }
        public bool IsApiUser { get; set; }
        public bool IsAdmin { get; set; }
        public string ApiKey { get; set; }

    }
}