﻿using DataAccess.BaseModels;
using Services.Interfaces.UserService;

namespace Services.Dtos
{
    public class UserForCreateDto : IUserForCreateDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public int UserTypeId { get; set; }
    }
}