﻿namespace Services.Dtos
{
    public class RequestForReturnDto
    {
        public int Id { get; set; }
        public long? LastStatusChangingTimestamp { get; set; }

        public string Status { get; set; }
    }
}