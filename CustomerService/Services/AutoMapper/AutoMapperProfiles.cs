﻿using Core.Enums;
using Core.Utils.Extensions;
using DataAccess;
using DataAccess.BaseModels;
using DataAccess.Entities;
using Services.Dtos;
using System;

namespace Services.AutoMapper
{
    public sealed class AutoMapperProfiles
    {
        public class UserProfile : AutoMapperProfile
        {
            public UserProfile()
            {
                CreateMap<IdentityModels.ApplicationUser, UserForListDto>();
                CreateMap<UserForCreateDto, IdentityModels.ApplicationUser>()
                    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
                    .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.UserTypeId))
                    .ForMember(dest => dest.CreationTimeStamp,
                        opt => opt.MapFrom(src => DateTime.UtcNow.ToUnixTimestamp()));
            }
        }

        public class UserTypeProfile : AutoMapperProfile
        {
            public UserTypeProfile()
            {
                CreateMap<UserTypeEntity, UserTypeBase>();
            }
        }

        public class UserStatusProfile : AutoMapperProfile
        {
            public UserStatusProfile()
            {
                CreateMap<UserStatusEntity, UserStatusBase>();
            }
        }

        public class RequestProfile: AutoMapperProfile 
        {
            public RequestProfile()
            {
                CreateMap<RequestForCreateDto, RequestBase>()
                    .ForMember(dest => dest.CreationTimeStamp,
                    opt => opt.MapFrom(src => DateTime.UtcNow.ToUnixTimestamp()))
                    .ForMember(dest => dest.StatusId, opt => opt.MapFrom(src => (int) RequestStatus.New));

                CreateMap<RequestBase, RequestForReturnDto>()
                    .ForMember(dest => dest.Status, opt => opt.MapFrom(src => ((RequestStatus) src.StatusId).ToString()));

                CreateMap<RequestBase, RequestEntity>();
            }
        }

    }
}