﻿using DataAccess.BaseModels;

namespace Services.Interfaces
{
    public interface IRequestHandler
    {
        IRequestHandler SetNext(IRequestHandler handler);
        bool Handle(RequestBase request);
    }
}