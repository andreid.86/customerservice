﻿namespace Services.Interfaces.QueueWatcher
{
    public interface IQueueWatcher
    {
        bool StartWatch();
        bool IsWatched();
        void StopWatch();
        void OnChanged();
    }
}