﻿namespace Services.Interfaces
{
    public interface IAssigneeRemover
    {
        void Remove(int requestId, int assigneeId);
    }
}