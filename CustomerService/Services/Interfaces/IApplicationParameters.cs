﻿namespace Services.Interfaces
{
    public interface IApplicationParameters
    {
        long WarningRequestTimeThresholdSeconds { get; }
        long CriticalRequestTimeThresholdSeconds { get; }
        uint LowerClientRequestRuntimeRangeSeconds { get; }
        uint UpperClientRequestRuntimeRangeSeconds { get; }
        uint RequestsQueueProcessingTimeoutSeconds { get; }
        uint FinalizerLoopServiceTimeoutSeconds { get; }
    }
}