﻿using DataAccess;
using Services.Dtos;

namespace Services.Interfaces.UserService
{
    public interface IUserFactory
    {
        IdentityModels.ApplicationUser CreateUser(UserForCreateDto request);
    }
}