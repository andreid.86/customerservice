﻿using DataAccess;
using DataAccess.BaseModels;
using Services.Dtos;
using System.Collections.Generic;

namespace Services.Interfaces.UserService
{
    public interface IUserService
    {
        IdentityModels.ApplicationUser GetUser(int userId);
        IdentityModels.ApplicationUser GetUserByApiKey(string apiKey);
        ICollection<IdentityModels.ApplicationUser> GetUsers();
        ICollection<IdentityModels.Role> GetRoles(int? userId = null);
        IdentityModels.ApplicationUser CreateUser(UserForCreateDto request);
        ICollection<UserTypeBase> GetUserTypes();
        ICollection<UserStatusBase> GetUserStatuses();
        ICollection<IdentityModels.ApplicationUser> GetUsersByType(int type);
        ICollection<IdentityModels.ApplicationUser> GetUsersByStatus(int status);
        bool DeleteUser(int userId);
        string ChangeApiKey(int userId);
    }
}