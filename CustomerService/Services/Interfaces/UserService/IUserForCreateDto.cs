﻿namespace Services.Interfaces.UserService
{
    public interface IUserForCreateDto
    {
        string Email { get; set; }
        string Password { get; set; }
    }
}