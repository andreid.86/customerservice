﻿using Core.Enums;
using DataAccess.BaseModels;
using Services.Dtos;
using System.Collections.Generic;
using DataAccess.Entities;

namespace Services.Interfaces.RequestService
{
    public interface IRequestService
    {
        RequestBase GetRequest(int id);
        RequestBase CreateRequest(RequestForCreateDto requestForCreateDto, int userId);
        void AssignRequest(int requestId, int assigneeId);
        ICollection<RequestBase> GetUnprocessedRequests();
        void UpdateRequestStatus(int requests, RequestStatus status);
        ICollection<RequestEntity> GetRequestsWithStatus(RequestStatus status);
        void DeleteRequest(int requestId);
    }
}