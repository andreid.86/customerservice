﻿namespace Services.Interfaces.Finalizer
{
    public interface IFinalizerService
    {
        void SetGetRequestsForCloseStrategy(IGetRequestsForClosingStrategy strategy);
        void SetFinalizerStrategy(IFinalizerStrategy strategy);
        void StartFinalizer();
    }
}