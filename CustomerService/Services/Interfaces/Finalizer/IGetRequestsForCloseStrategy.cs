﻿using DataAccess.Entities;
using System.Collections.Generic;

namespace Services.Interfaces.Finalizer
{
    public interface IGetRequestsForClosingStrategy
    {
        ICollection<RequestEntity> GetRequestsForClosing();
    }
}