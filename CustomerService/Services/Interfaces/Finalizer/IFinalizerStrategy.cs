﻿using DataAccess.Entities;
using System.Collections.Generic;

namespace Services.Interfaces.Finalizer
{
    public interface IFinalizerStrategy
    {
        void StartFinalize(ICollection<RequestEntity> requests);
    }
}