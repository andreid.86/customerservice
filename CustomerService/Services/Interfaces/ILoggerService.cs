﻿using System;

namespace Services.Interfaces
{
    public interface ILoggerService
    {
        void LogInfo(string message);
        void LogError(string message);
        void LogException(Exception ex);
    }
}