﻿using System;

namespace Services.Interfaces.Reports
{
    public interface IReportFilter
    {
        DateTime? Since { get; set; }
        DateTime? Until { get; set; }
    }
}