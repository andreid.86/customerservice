﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Interfaces.Reports
{
    public interface IReport<TRow, in TFilter>
    {
        string ReportName { get; }
        Task GetReportDataAsync(TFilter filter);
        ICollection<TRow> Rows { get; set; }
    }
}