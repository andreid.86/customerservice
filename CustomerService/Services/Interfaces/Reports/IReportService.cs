﻿using System.Threading.Tasks;
using Services.Implementations.ReportService.Reports.RequestsReport;
using Services.Implementations.ReportService.Reports.UsersReport;

namespace Services.Interfaces.Reports
{
    public interface IReportService
    {
        Task<RequestReport> GetRequestsReportAsync(RequestReportFilter filter);
        Task<UsersReport> GetUsersReportAsync(UsersReportFilter filter);
    }
}