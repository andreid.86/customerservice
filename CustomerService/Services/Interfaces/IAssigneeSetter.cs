﻿using Core.Enums;
using DataAccess.BaseModels;

namespace Services.Interfaces
{
    public interface IAssigneeSetter
    {
        int? TrySetAssignee(RequestBase request, UserType userType, long requestAgeThreshold);
    }
}