﻿using System.Threading.Tasks;
using Services.Implementations.AuthService;

namespace Services.Interfaces.AuthService
{
    public interface IAuthService
    {
        bool IsApiKeyValid(string apiKey);
        Task<bool> IsApiKeyValidAsync(string apiKey);
        Task<UserDetails> GetUserDetailsAsync(string token);
    }
}