﻿using DataAccess.BaseModels;

namespace Services.Interfaces.QueueService
{
    public interface IQueueService
    {
        void Enqueue(RequestBase request);
        RequestBase Peek();
        RequestBase Dequeue();
        int Length { get; }
        void UnPeek();
    }
}