﻿using Core.Enums;
using DataAccess.BaseModels;
using Services.Interfaces;

namespace Services.Implementations.RequestHandlers
{
    public sealed class SupportHandler : AbstractRequestHandler
    {
        private readonly IAssigneeSetter _assigneeSetter;

        public SupportHandler(IAssigneeSetter assigneeSetter)
        {
            _assigneeSetter = assigneeSetter;
        }

        public override bool Handle(RequestBase request)
        {
            const int supportRequestAgeThreshold = 0;
            
            if (_assigneeSetter.TrySetAssignee(request, UserType.Support, supportRequestAgeThreshold) == null)
            {
                return base.Handle(request);
            }

            return true;
        }
    }
}