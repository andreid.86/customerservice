﻿using Core.Enums;
using DataAccess.BaseModels;
using Services.Interfaces;

namespace Services.Implementations.RequestHandlers
{
    public sealed class ExecutiveHandler : AbstractRequestHandler
    {
        private IAssigneeSetter _assigneeSetter;
        public ExecutiveHandler(IAssigneeSetter assigneeSetter)
        {
            _assigneeSetter = assigneeSetter;
        }

        public override bool Handle(RequestBase request)
        {
            var executiveRequestAgeThreshold = ApplicationParameters.Instance.CriticalRequestTimeThresholdSeconds;

            if (_assigneeSetter.TrySetAssignee(request, UserType.Executive, executiveRequestAgeThreshold) == null)
            {
                return false;
            }

            return true;
        }
    }
}