﻿using Core.Enums;
using DataAccess.BaseModels;
using Services.Interfaces;

namespace Services.Implementations.RequestHandlers
{
    public sealed class ManagerHandler : AbstractRequestHandler
    {
        private readonly IAssigneeSetter _assigneeSetter;

        public ManagerHandler(IAssigneeSetter assigneeSetter)
        {
            _assigneeSetter = assigneeSetter;
        }

        public override bool Handle(RequestBase request)
        {
            var managerRequestAgeThreshold = ApplicationParameters.Instance.WarningRequestTimeThresholdSeconds;

            if (_assigneeSetter.TrySetAssignee(request, UserType.Manager, managerRequestAgeThreshold) == null)
            {
                return base.Handle(request);
            }

            return true;

        }
    }
}