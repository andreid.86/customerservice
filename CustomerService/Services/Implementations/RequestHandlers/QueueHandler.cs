﻿using DataAccess.BaseModels;
using Services.Interfaces.QueueService;

namespace Services.Implementations.RequestHandlers
{
    public sealed class QueueHandler : AbstractRequestHandler
    {
        private readonly IQueueService _queueService;

        public QueueHandler(IQueueService queueService)
        {
            _queueService = queueService;
        }

        public override bool Handle(RequestBase request)
        {
            _queueService.Enqueue(request);
            return true;
        }
    }
}