﻿using DataAccess.BaseModels;
using Services.Interfaces;

namespace Services.Implementations.RequestHandlers
{
    public abstract class AbstractRequestHandler : IRequestHandler
    {
        private IRequestHandler _nextHandler;
        public IRequestHandler SetNext(IRequestHandler handler)
        {
            _nextHandler = handler;
            return handler;
        }

        public virtual bool Handle(RequestBase request)
        {
            return _nextHandler?.Handle(request) ?? false;
        }
    }
}