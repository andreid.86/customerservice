﻿using Core.Enums;
using DataAccess.BaseModels;
using Services.Interfaces;
using Services.Interfaces.QueueService;
using Services.Interfaces.RequestService;

namespace Services.Implementations.RequestHandlers
{
    public sealed class RequestManager
    {
        private readonly IRequestHandler _supportHandler;
        private readonly IRequestHandler _managerHandler;
        private readonly IRequestHandler _executiveHandler;
        private readonly IRequestHandler _queueHandler;
        private readonly IRequestService _requestService;
        private readonly IQueueService _queueService;

        public RequestManager(IQueueService queueService, 
            IRequestService requestService,
            SupportHandler supportHandler,
            ManagerHandler managerHandler,
            ExecutiveHandler executiveHandler,
            QueueHandler queueHandler)
        {
            _requestService = requestService;
            _queueService = queueService;
            _supportHandler = supportHandler;
            _managerHandler = managerHandler;
            _executiveHandler = executiveHandler;
            _queueHandler = queueHandler;
        }

        public void ProcessNewRequest(RequestBase request)
        {
            _supportHandler.SetNext(_queueHandler);
            _supportHandler.Handle(request);
        }

        public void ProcessQueue(IRequestHandler handler = null)
        {
            if (_queueService.Length < 1) return;

            bool processed;

            _supportHandler.SetNext(_managerHandler).SetNext(_executiveHandler);

            var requestHandler = handler ?? _supportHandler;

            do
            {
                try
                {
                    var request = _queueService.Peek();
                    processed = requestHandler.Handle(request);
                    if (processed) _queueService.Dequeue();
                }
                catch
                {
                    processed = false;
                }
                finally
                {
                    _queueService.UnPeek();
                }

            } while (_queueService.Length > 0 && processed);


        }

        public bool CloseRequest(int requestId)
        {
            try
            {
                _requestService.UpdateRequestStatus(requestId, RequestStatus.Closed);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}