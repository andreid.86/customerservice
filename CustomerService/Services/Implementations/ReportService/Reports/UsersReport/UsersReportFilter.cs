﻿using System.Collections.Generic;

namespace Services.Implementations.ReportService.Reports.UsersReport
{
    public class UsersReportFilter
    {
        public IList<int> UserTypes { get; set; }
    }
}