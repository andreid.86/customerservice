﻿using System;
using System.Data;
using Dapper;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Services.Implementations.ReportService.Reports.UsersReport
{
    public sealed class UsersReport : AbstractReport<UsersReportRow, UsersReportFilter>
    {
        public override string ReportName => "Users report";

        // Compromise approach. Need to encapsulate logic into stored procedure 
        protected override string SqlQueryString =>
            $@"SELECT 
	            U.Email AS Email,
	            S.Name AS Status
            FROM 
	            USERS U
            LEFT JOIN 
	            UserStatuses S ON U.StatusId = S.Id
            WHERE 
	            (U.DeletionTimeStamp IS NULL)
	            AND (U.LockOutEndDateUtc IS NULL OR U.LockOutEndDateUtc < @now)
	            AND (U.TypeId IN @userTypeIds)
            ORDER BY
	            CASE WHEN U.StatusId IS NULL THEN 1 ELSE 0 END, U.StatusId";

        public override async Task GetReportDataAsync(UsersReportFilter filter)
        {
            var requestParameters = GetRequestParameters(filter);

            using (var conn = new SqlConnection(ConnectionString))
            {
                var query = await conn.QueryAsync<UsersReportRow>(SqlQueryString, requestParameters, commandTimeout: CommandTimeout);
                var queryResult = query.ToList();
                Rows = queryResult;
            }
        }

        private static DynamicParameters GetRequestParameters(UsersReportFilter filter)
        {
            var requestParameters = new DynamicParameters();
            requestParameters.Add("@userTypeIds", filter.UserTypes);
            requestParameters.Add("@now", DateTime.UtcNow, DbType.DateTime);

            return requestParameters;
        }

    }
}