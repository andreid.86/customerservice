﻿namespace Services.Implementations.ReportService.Reports.UsersReport
{
    public class UsersReportRow
    {
        public string Email { get; set; }
        public string Status { get; set; }
    }
}