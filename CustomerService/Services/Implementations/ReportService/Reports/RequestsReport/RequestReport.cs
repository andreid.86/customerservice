﻿using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Core.Enums;
using Core.Utils.Extensions;
using Dapper;

namespace Services.Implementations.ReportService.Reports.RequestsReport
{
    public sealed class RequestReport : AbstractReport<RequestReportRow, RequestReportFilter>
    {
        private const int DefaultCountConst = 20;
        public override string ReportName => $"Last {DefaultCountConst} Requests";

        // Compromise approach. Need to encapsulate logic into stored procedure 
        protected override string SqlQueryString =>
            $@"SELECT TOP (@count)
	                    R.Id AS Id,
	                    U.Email AS Assignee,
                        C.Email AS Creator,
	                    DATEADD(S, R.CreationTimeStamp, '1970-01-01') AS CreatedDateTime,
	                    CASE
		                    WHEN R.StatusId = {(int)RequestStatus.Closed} THEN DATEADD(S, R.LastStatusChangingTimestamp, '1970-01-01')
		                    ELSE NULL
	                    END AS ClosedDateTime,
	                    CASE
		                    WHEN R.StatusId = {(int)RequestStatus.Closed} THEN R.LastStatusChangingTimestamp - R.CreationTimeStamp
		                    ELSE NULL 
	                    END AS Duration,
	                    RS.Name AS Status
	                    
                    FROM 
	                    [dbo].Requests R
                    LEFT JOIN 
	                    [dbo].Users U ON U.Id = R.AssigneeId
                    LEFT JOIN 
	                    [dbo].Users C ON C.Id = R.CreatorId
                    LEFT JOIN 
	                    [dbo].RequestStatuses RS ON RS.ID = R.StatusId
                        
                    WHERE
	                    ((@since IS NULL) OR (@since <= R.CreationTimeStamp))
	                    AND ((@until IS NULL) OR (R.CreationTimeStamp >= @until))

                    ORDER BY 
                        CreatedDateTime DESC";
        
        public override  async Task GetReportDataAsync(RequestReportFilter filter)
        {
            var requestParameters = GetRequestParameters(filter);
            
            using (var conn = new SqlConnection(ConnectionString))
            {
                var query = await conn.QueryAsync<RequestReportRow>(SqlQueryString, requestParameters, commandTimeout:CommandTimeout);
                var queryResult = query.ToList();
                queryResult.ForEach(r =>
                {
                    r.Duration /= (decimal) Core.Constants.DateTime.SecondsPerMinute;
                    r.CreatedDateTime = r.CreatedDateTime.ToLocalTime();
                });
                Rows = queryResult;
            }
           
        }

        private static DynamicParameters GetRequestParameters(RequestReportFilter filter)
        {
            var since = filter.Since?.ToUnixTimestamp();
            var until = filter.Until?.ToUnixTimestamp();
            var count = filter.Count ?? DefaultCountConst;

            var requestParameters = new DynamicParameters();
                requestParameters.Add("@since", since, DbType.Int64);
                requestParameters.Add("@until", until, DbType.Int64);
                requestParameters.Add("@count", count, DbType.Int64);

            return requestParameters;
        }
    }
}