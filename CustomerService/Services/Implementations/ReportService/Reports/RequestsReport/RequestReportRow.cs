﻿using System;

namespace Services.Implementations.ReportService.Reports.RequestsReport
{
    public class RequestReportRow
    {
        public int Id { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public string Assignee { get; set; }
        public string Creator { get; set; }
        public string Status { get; set; }
        public DateTime? ClosedDateTime { get; set; }
        public decimal Duration { get; set; }
    }
}