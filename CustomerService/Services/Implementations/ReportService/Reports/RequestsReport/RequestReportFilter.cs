﻿using System;
using Services.Interfaces.Reports;

namespace Services.Implementations.ReportService.Reports.RequestsReport
{
    public class RequestReportFilter : IReportFilter
    {
        public DateTime? Since { get; set; }
        public DateTime? Until { get; set; }
        public int? Count { get; set; }
    }
}