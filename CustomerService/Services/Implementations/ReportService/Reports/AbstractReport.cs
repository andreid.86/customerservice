﻿using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Services.Interfaces.Reports;

namespace Services.Implementations.ReportService.Reports
{
    public abstract class AbstractReport<TRow, TFilter> : IReport<TRow, TFilter>  where TRow: class where TFilter: class
    {
        protected string ConnectionString = ConfigurationManager.ConnectionStrings[Core.Constants.DataContext.Name].ToString();
        protected int CommandTimeout = Core.Constants.DataContext.CommandTimeout;

        public abstract string ReportName { get; }

        public abstract Task GetReportDataAsync(TFilter filter);

        public ICollection<TRow> Rows { get; set; }
        protected abstract string SqlQueryString { get; }
    }
}