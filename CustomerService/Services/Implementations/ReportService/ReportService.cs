﻿using System.Threading.Tasks;
using Services.Implementations.ReportService.Reports.RequestsReport;
using Services.Implementations.ReportService.Reports.UsersReport;
using Services.Interfaces.Reports;

namespace Services.Implementations.ReportService
{
    public sealed class ReportService : IReportService
    {
        public async Task<RequestReport> GetRequestsReportAsync(RequestReportFilter filter)
        {
            var report = new RequestReport();
            await report.GetReportDataAsync(filter);
            return report;
        }

        public async Task<UsersReport> GetUsersReportAsync(UsersReportFilter filter)
        {
            var report = new UsersReport();
            await report.GetReportDataAsync(filter);
            return report;
        }
    }
}