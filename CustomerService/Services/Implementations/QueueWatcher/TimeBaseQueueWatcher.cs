﻿using Services.Implementations.RequestHandlers;
using System.Threading;

namespace Services.Implementations.QueueWatcher
{
    public sealed class TimeBaseQueueWatcher : AbstractQueueWatcher
    {
        private readonly uint _timeout = ApplicationParameters.Instance.RequestsQueueProcessingTimeoutSeconds *
                               Core.Constants.DateTime.MillisecondsPerSecond;
        
        private bool _isWatched;
        private System.Threading.Timer _timer;


        public override bool StartWatch()
        {
            _timer = new System.Threading.Timer((e) =>
                {
                    TimerOnChangeCallBack();
                },
                null,
                _timeout,
                Timeout.Infinite);

            _isWatched = true;

            return _isWatched;

        }

        public override bool IsWatched()
        {
            return _isWatched;
        }

        public override void StopWatch()
        {
            _timer?.Dispose();
            _isWatched = false;
        }


        private void TimerOnChangeCallBack()
        {
            _timer.Change(_timeout, Timeout.Infinite);
            base.OnChanged();
        }

        public TimeBaseQueueWatcher(RequestManager requestManager) : base(requestManager)
        {
            StartWatch();
        }
    }
}