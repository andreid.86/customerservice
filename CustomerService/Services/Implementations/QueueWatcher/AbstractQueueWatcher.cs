﻿using Services.Implementations.RequestHandlers;
using Services.Interfaces.QueueWatcher;

namespace Services.Implementations.QueueWatcher
{
    public abstract class AbstractQueueWatcher : IQueueWatcher
    {
        private static RequestManager _requestManager;

        protected AbstractQueueWatcher(RequestManager requestManager)
        {
            _requestManager = requestManager;
        }
        

        public abstract bool StartWatch();

        public abstract bool IsWatched();

        public abstract void StopWatch();

        public void OnChanged()
        {
            _requestManager.ProcessQueue();
        }
    }

}