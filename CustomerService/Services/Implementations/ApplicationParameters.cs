﻿using Services.Interfaces;
using System.Configuration;

namespace Services.Implementations
{
    public sealed class ApplicationParameters : IApplicationParameters
    {
        private static ApplicationParameters instance;

        private const long DefaultWarningRequestTimeThresholdSecondsConstant = 600;
        private const long DefaultCriticalRequestTimeThresholdSecondsConstant = 1200;
        private const uint DefaultLowerClientRequestRuntimeRangeSecondsConstant = 30;
        private const uint DefaultUpperClientRequestRuntimeRangeSecondsConstant = 300;
        private const uint DefaultRequestsQueueProcessingTimeoutSecondsConstant = 5;
        private const uint DefaultFinalizerLoopServiceTimeoutSecondConstant = 30;

        // Jon Skeet fully lazy instantiation singleton instantiation
        // https://csharpindepth.com/articles/singleton
        public static ApplicationParameters Instance => Helper.instance;

        private ApplicationParameters()
        {

        }

        /// <summary>
        /// Determines how much time in seconds must elapse
        /// before a request from the queue moves to the second level
        /// </summary>
        public long WarningRequestTimeThresholdSeconds
        {
            get
            {
                var result = int.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.WarningRequestTimeThresholdSeconds], out var warningRequestTimeThresholdSeconds) 
                    ? warningRequestTimeThresholdSeconds
                    : DefaultWarningRequestTimeThresholdSecondsConstant;
                return result;
            }
        }

        /// <summary>
        /// Determines how much time in seconds must elapse
        /// before a request from the queue moves to the third level
        /// </summary>
        public long CriticalRequestTimeThresholdSeconds
        {
            get
            {
                var result = int.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.CriticalRequestTimeThresholdSeconds], out var criticalRequestTimeThresholdSeconds)
                    ? criticalRequestTimeThresholdSeconds
                    : DefaultCriticalRequestTimeThresholdSecondsConstant;
                return result;
            }
        }

        /// <summary>
        /// Defines the lower end of the client request runtime range
        /// </summary>
        public uint LowerClientRequestRuntimeRangeSeconds
        {
            get
            {
                var result = uint.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.LowerClientRequestRuntimeRangeSeconds], out var lowerClientRequestRuntimeRangeSeconds)
                    ? lowerClientRequestRuntimeRangeSeconds
                    : DefaultLowerClientRequestRuntimeRangeSecondsConstant;
                return result;
            }
        }

        /// <summary>
        /// Defines the upper end of the client request runtime range
        /// </summary>
        public uint UpperClientRequestRuntimeRangeSeconds {
            get
            {
                var result = uint.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.UpperClientRequestRuntimeRangeSeconds], out var upperClientRequestRuntimeRangeSeconds)
                    ? upperClientRequestRuntimeRangeSeconds
                    : DefaultUpperClientRequestRuntimeRangeSecondsConstant;
                return result;
            }
        }

        /// <summary>
        /// Defines the timeout of the clients request queue checking
        /// </summary>
        public uint RequestsQueueProcessingTimeoutSeconds
        {
            get
            {
                var result = uint.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.RequestsQueueProcessingTimeoutSeconds], out var requestsQueueProcessingTimeoutSeconds)
                    ? requestsQueueProcessingTimeoutSeconds
                    : DefaultRequestsQueueProcessingTimeoutSecondsConstant;
                return result;
            }
        }

        /// <summary>
        /// Defines the timeout of the processed clients request checking
        /// </summary>
        public uint FinalizerLoopServiceTimeoutSeconds
        {
            get
            {
                var result = uint.TryParse(ConfigurationManager.AppSettings[Core.Constants.Parameters.FinalizerLoopServiceTimeoutSeconds], out var finalizerLoopServiceTimeoutSeconds)
                    ? finalizerLoopServiceTimeoutSeconds
                    : DefaultFinalizerLoopServiceTimeoutSecondConstant;
                return result;
            }
        }


        /// <summary>
        /// Explicit static constructor to tell C# compiler
        /// not to mark type as beforefieldinit
        /// </summary>
        private class Helper
        {
            static Helper()
            {

            }

            internal static readonly ApplicationParameters instance = new ApplicationParameters();
        }
    }
}