﻿using AutoMapper;
using Core.Enums;
using Core.Utils.Extensions;
using DataAccess;
using DataAccess.BaseModels;
using DataAccess.Entities;
using Services.Dtos;
using Services.Interfaces.RequestService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Services.Implementations.RequestService
{
    public sealed class RequestService : BaseService, IRequestService
    {
        private readonly ApplicationDataContext _dbContext;
        public RequestService(IMapper mapper, ApplicationDataContext dbContext) : base(mapper)
        {
            _dbContext = dbContext;
        }

        public RequestBase GetRequest(int id)
        {
            var result = _dbContext.Requests.Single(r => r.Id == id);
            return result;
        }

        public RequestBase CreateRequest(RequestForCreateDto requestForCreateDto, int userId)
        {

            var request = Mapper.Map<RequestBase>(requestForCreateDto);

            request.CreatorId = userId;

            var requestEntity = Mapper.Map<RequestEntity>(request);


            _dbContext.Requests.Add(requestEntity);

            var result = _dbContext.SaveChanges();

            request.Id = requestEntity.Id;

            if (result < 1)
                throw new Exception(Core.Messages.Errors.CreateRequestError);

            return request;

        }

        public void AssignRequest(int requestId, int assigneeId)
        {

            var requestForUpdate = _dbContext.Requests.Single(r => r.Id == requestId);
            requestForUpdate.LastStatusChangingTimestamp = DateTime.UtcNow.ToUnixTimestamp();
            requestForUpdate.AssigneeId = assigneeId;
            requestForUpdate.StatusId = (int)RequestStatus.InProgress;

            var assignee = _dbContext.Users.Single(u => u.Id == assigneeId);

            assignee.StatusId = (int)UserStatus.Busy;

            if (_dbContext.SaveChanges() <= 0)
                throw new Exception(Core.Messages.Errors.AssignRequestError);

        }

        public ICollection<RequestBase> GetUnprocessedRequests()
        {

            var result = _dbContext.Requests.Where(r =>
                    r.DeletionTimeStamp == null && r.StatusId != (int)RequestStatus.Closed && r.AssigneeId == null)
                .ToArray();
            return result;

        }

        public void UpdateRequestStatus(int requestsId, RequestStatus status)
        {

            var requestForUpdate = _dbContext.Requests.Single(r => r.Id == requestsId);
            requestForUpdate.StatusId = (int)status;
            requestForUpdate.LastStatusChangingTimestamp = DateTime.UtcNow.ToUnixTimestamp();

            var requestAssignee = _dbContext.Users.Single(u => u.Id == requestForUpdate.AssigneeId);

            if (status == RequestStatus.Closed)
                requestAssignee.StatusId = (int)UserStatus.Free;

            if (_dbContext.SaveChanges() <= 0)
                throw new Exception(Core.Messages.Errors.UpdateRequestStatusError);

        }

        public ICollection<RequestEntity> GetRequestsWithStatus(RequestStatus status)
        {

            var result = _dbContext.Requests
                .Include(r => r.Assignee)
                .Where(r => r.DeletionTimeStamp == null && r.StatusId == (int)status)
                .ToArray();
            return result;

        }

        public void DeleteRequest(int requestId)
        {

            var requestForDelete = _dbContext.Requests.Single(r => r.Id == requestId);

            if (requestForDelete.StatusId == (int)RequestStatus.InProgress)
                throw new Exception(Core.Messages.Errors.DeleteProcessingRequestError);

            requestForDelete.DeletionTimeStamp = DateTime.UtcNow.ToUnixTimestamp();
            if (_dbContext.SaveChanges() <= 0)
                throw new Exception(Core.Messages.Errors.DeleteRequestError);
        }

    }

}