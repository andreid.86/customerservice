﻿using DataAccess.BaseModels;
using Services.Interfaces.QueueService;
using Services.Interfaces.RequestService;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Services.Implementations.QueueService
{
    public sealed class QueueService : IQueueService
    {
        private readonly IRequestService _requestService;
        private Queue<RequestBase> _queue;

        private readonly object _sync = new object();

        public QueueService(IRequestService requestService)
        {
            _requestService = requestService;
            InitQueue();
        }


        public void Enqueue(RequestBase request)
        {
            Monitor.Enter(_sync);

            _queue.Enqueue(request);

            Monitor.Exit(_sync);
        }

        public RequestBase Peek()
        {

            Monitor.Enter(_sync);

            var result = _queue.Peek();

            return result;
        }

        public void UnPeek()
        {
            Monitor.Exit(_sync);
        }

        public RequestBase Dequeue()
        {
            Monitor.Enter(_sync);

            var result = _queue.Dequeue();

            Monitor.Exit(_sync);

            return result;
        }

        public int Length
        {
            get
            {
                Monitor.Enter(_sync);

                var result = _queue.Count;

                Monitor.Exit(_sync);

                return result;
            }
        }


        /// <summary>
        /// Get all unprocessed requests From db and add the to the queue
        /// </summary>
        private void InitQueue()
        {
            var unprocessedRequests = _requestService.GetUnprocessedRequests().OrderBy(r => r.CreationTimeStamp);
            _queue = new Queue<RequestBase>(unprocessedRequests);
        }

    }
}