﻿using AutoMapper;
using System.Configuration;

namespace Services.Implementations
{
    public class BaseService
    {
        protected static string ConnectionString = ConfigurationManager.ConnectionStrings[Core.Constants.DataContext.Name].ToString();
        protected static int CommandTimeout = Core.Constants.DataContext.CommandTimeout;

        protected readonly IMapper Mapper;

        public BaseService(IMapper mapper)
        {
            Mapper = mapper;
        }
    }
}