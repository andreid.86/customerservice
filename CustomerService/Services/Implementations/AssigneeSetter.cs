﻿using Core.Enums;
using Core.Utils.Extensions;
using DataAccess.BaseModels;
using Services.Interfaces;
using Services.Interfaces.RequestService;
using Services.Interfaces.UserService;
using System.Linq;

namespace Services.Implementations
{
    public sealed class AssigneeSetter : IAssigneeSetter
    {
        private readonly IUserService _userService;
        private readonly IRequestService _requestService;

        public AssigneeSetter(IUserService userService, IRequestService requestService)
        {
            _userService = userService;
            _requestService = requestService;
        }

        public int? TrySetAssignee(RequestBase request, UserType userType, long requestAgeThreshold)
        {
            var requestAge = request.GetRequestAge();

            var usersByType = _userService.GetUsersByType((int)userType);

            // Compromise approach
            // A hard-coded userType checking is used
            // If the user is and executive, then it is not filter it by status
            // Otherwise, the request will not be passed down the chain and will remain unprocessed

            var freeUsers = userType != UserType.Executive ? usersByType.Where(s => s.StatusId != (int) UserStatus.Busy).ToList() : usersByType;

            int? assigneeId = null;

            if (freeUsers.Any() && requestAgeThreshold <= requestAge)
            {
                var assignee = freeUsers.TakeRandom();

                try
                {
                    _requestService.AssignRequest(request.Id, assignee.Id);
                    assigneeId = assignee.Id;
                }
                catch
                {
                    // Do nothing
                }
             
            }
            
            return assigneeId;
        }
    }
}