﻿namespace Services.Implementations.AuthService
{
    public class UserDetails
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string ApiKey { get; set; }
    }
}