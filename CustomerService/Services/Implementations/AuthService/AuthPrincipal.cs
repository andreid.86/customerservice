﻿using System.Security.Principal;

namespace Services.Implementations.AuthService
{
    public sealed class AuthPrincipal : GenericPrincipal
    {
        public AuthPrincipal(IIdentity identity, string[] roles) : base(identity, roles)
        {
        }

        public UserDetails UserDetails { get; set; }
    }
}