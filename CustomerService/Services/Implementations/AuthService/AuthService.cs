﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Core.Utils.Extensions;
using Dapper;
using Services.Interfaces.AuthService;

namespace Services.Implementations.AuthService
{
    public sealed class AuthService : IAuthService
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings[Core.Constants.DataContext.Name].ToString();

        private readonly string _sqlQueryForAuth = $"SELECT ApiKey from {Core.Constants.TableNames.UsersTableName} " +
                                                     $"WHERE " +
                                                     $"(ApiKey IS NOT NULL)" +
                                                     $"AND (DeletionTimeStamp IS NULL) " +
                                                     $"AND (LockOutEndDateUtc IS NULL OR LockOutEndDateUtc < @now)";
        
        private readonly string _sqlQueryForGetUserDetails = $"SELECT Id AS Id, Email AS Email, ApiKey AS ApiKey " +
                                                             $"FROM {Core.Constants.TableNames.UsersTableName} " +
                                                             $"WHERE " +
                                                             $"(ApiKey = @token COLLATE database_default)" +
                                                             $"AND (DeletionTimeStamp IS NULL) " +
                                                             $"AND (LockOutEndDateUtc IS NULL OR LockOutEndDateUtc < @now)";
        
        private readonly DateTime _currentDateTime = DateTime.UtcNow;

        public bool IsApiKeyValid(string apiKey)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var apiKeys = conn
                    .Query<string>(_sqlQueryForAuth, 
                        new { @now = _currentDateTime },
                        commandTimeout: Core.Constants.DataContext.CommandTimeout).ToList();

                return !apiKeys.IsNullOrEmpty() && apiKeys.Contains(apiKey, StringComparer.OrdinalIgnoreCase); 
            }
        }

        public async Task<UserDetails> GetUserDetailsAsync(string token)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var query = await conn
                    .QueryAsync<UserDetails>(_sqlQueryForGetUserDetails,
                        new { @token = token, @now = _currentDateTime },
                        commandTimeout: Core.Constants.DataContext.CommandTimeout);

                var userDetails = query.Single();

                return userDetails;
            }
        }

        public async Task<bool> IsApiKeyValidAsync(string apiKey)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                var query = await conn
                    .QueryAsync<string>(_sqlQueryForAuth,
                        new {@now = _currentDateTime},
                        commandTimeout: Core.Constants.DataContext.CommandTimeout);
                var apiKeys = query.ToList();

                return !apiKeys.IsNullOrEmpty() && apiKeys.Contains(apiKey, StringComparer.OrdinalIgnoreCase);
            }
        }
    }
}