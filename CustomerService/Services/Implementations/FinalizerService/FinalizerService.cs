﻿using DataAccess.Entities;
using Services.Interfaces.Finalizer;
using System.Collections.Generic;
using System.Threading;

namespace Services.Implementations.FinalizerService
{
    public sealed class FinalizerService : IFinalizerService
    {
        private IGetRequestsForClosingStrategy _getRequestsForClosingStrategy;
        private IFinalizerStrategy _finalizerStrategy;

        private readonly uint _timeout = ApplicationParameters.Instance.FinalizerLoopServiceTimeoutSeconds *
                                         Core.Constants.DateTime.MillisecondsPerSecond;

        private System.Threading.Timer _timer;

        public FinalizerService(IGetRequestsForClosingStrategy getRequestsForClosingStrategy, IFinalizerStrategy finalizerStrategy)
        {
            _getRequestsForClosingStrategy = getRequestsForClosingStrategy;
            _finalizerStrategy = finalizerStrategy;
            StartFinalizer();
        }

        public void SetGetRequestsForCloseStrategy(IGetRequestsForClosingStrategy strategy)
        {
            _getRequestsForClosingStrategy = strategy;
        }

        public void SetFinalizerStrategy(IFinalizerStrategy strategy)
        {
            _finalizerStrategy = strategy;
        }

        public ICollection<RequestEntity> GetRequestsForClosing()
        {
            var result = _getRequestsForClosingStrategy.GetRequestsForClosing();
            return result;
        }

        public void StartFinalizer()
        {
            _timer = new System.Threading.Timer((e) =>
                {
                    TimerOnChangeCallBack();
                },
                null,
                _timeout,
                Timeout.Infinite);
        }

        private void TimerOnChangeCallBack()
        {
            var requestsForClosing = GetRequestsForClosing();
            _finalizerStrategy.StartFinalize(requestsForClosing);
            _timer.Change(_timeout, Timeout.Infinite);
        }
    }
}