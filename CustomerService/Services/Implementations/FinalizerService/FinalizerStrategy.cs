﻿using Core.Enums;
using Core.Utils.Extensions;
using DataAccess.Entities;
using Services.Implementations.RequestHandlers;
using Services.Interfaces;
using Services.Interfaces.Finalizer;
using System.Collections.Generic;

namespace Services.Implementations.FinalizerService
{
    public sealed class FinalizerStrategy : IFinalizerStrategy
    {
        private readonly IAssigneeSetter _assigneeSetter;
        private readonly RequestManager _requestManager;


        public FinalizerStrategy(IAssigneeSetter assigneeSetter, RequestManager requestManager)
        {
            _assigneeSetter = assigneeSetter;
            _requestManager = requestManager;
        }


        public void StartFinalize(ICollection<RequestEntity> requests)
        {

            requests.ForEach(r =>
            {
                var assigneeId = r.AssigneeId;
                var assingneeTypeId = r.Assignee.TypeId;

                if (assigneeId == null) return;

                if (_requestManager.CloseRequest(r.Id))
                {
                    if (assingneeTypeId == (int) UserType.Support)
                    {
                        var requestHandler = new SupportHandler(_assigneeSetter);
                        _requestManager.ProcessQueue(requestHandler);
                    }
                };

                
            });
        }
    }
}