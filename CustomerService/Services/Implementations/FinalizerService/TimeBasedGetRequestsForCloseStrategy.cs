﻿using Core.Enums;
using Core.Utils.Extensions;
using DataAccess.Entities;
using Services.Interfaces.Finalizer;
using Services.Interfaces.RequestService;
using System;
using System.Collections.Generic;

namespace Services.Implementations.FinalizerService
{
    public sealed class TimeBasedGetRequestsForCloseStrategy : IGetRequestsForClosingStrategy
    {
        private readonly IRequestService _requestService;

        public TimeBasedGetRequestsForCloseStrategy(IRequestService requestService)
        {
            _requestService = requestService;
        }

        public ICollection<RequestEntity> GetRequestsForClosing()
        {
            
            var random = new Random();
            var lowerClientRequestRuntimeRange = ApplicationParameters.Instance.LowerClientRequestRuntimeRangeSeconds;
            var upperClientRequestRuntimeRange = ApplicationParameters.Instance.UpperClientRequestRuntimeRangeSeconds;

            var result = new List<RequestEntity>();

            var requests = _requestService.GetRequestsWithStatus(RequestStatus.InProgress);
            
            foreach (var request in requests)
            {
                var randomRuntime = random.Next((int) lowerClientRequestRuntimeRange, (int) upperClientRequestRuntimeRange);
                
                var timeSinceLastStatusChanged = DateTime.UtcNow.ToUnixTimestamp() - request.LastStatusChangingTimestamp;
                
                if (timeSinceLastStatusChanged >= randomRuntime)
                    result.Add(request);
            }

            return result;
        }
    }
}