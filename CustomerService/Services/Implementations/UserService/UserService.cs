﻿using AutoMapper;
using Core.Enums;
using Core.Utils;
using Core.Utils.Extensions;
using DataAccess;
using DataAccess.BaseModels;
using Services.Dtos;
using Services.Interfaces.UserService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Services.Implementations.UserService
{
    public sealed class UserService : BaseService, IUserService
    {

        private readonly IUserFactory _userFactory;
        private readonly ApplicationDataContext _dbContext;
        private readonly IdentityConfig.ApplicationRoleManager _roleManager;

        public UserService(IMapper mapper, IUserFactory userFactory, ApplicationDataContext dbContext, IdentityConfig.ApplicationRoleManager roleManager) : base(mapper)
        {
            _userFactory = userFactory;
            _dbContext = dbContext;
            _roleManager = roleManager;
        }

        public IdentityModels.ApplicationUser GetUser(int userId)
        {

            var user = _dbContext.Users
                .Include(u => u.Roles)
                .Single(u => u.Id == userId && u.DeletionTimeStamp == null);

            return user;

        }

        public IdentityModels.ApplicationUser GetUserByApiKey(string apiKey)
        {

            var user = _dbContext.Users
                .Include(u => u.Roles)
                .Single(u => (u.ApiKey == apiKey) && u.DeletionTimeStamp == null);

            return user;

        }

        public ICollection<IdentityModels.ApplicationUser> GetUsers()
        {

            var users = _dbContext.Users.Include(u => u.Roles)
                .Where(u => u.DeletionTimeStamp == null)
                .ToList();

            return users;

        }

        public ICollection<IdentityModels.Role> GetRoles(int? userId = null)
        {

            var allRoles = _roleManager.Roles.ToList();

            ICollection<IdentityModels.Role> result;
            if (userId == null)
            {
                result = allRoles;
            }
            else
            {
                var userRoleIds = _dbContext.Users.Include(u => u.Roles)
                    .First(u => u.Id == userId).Roles
                    .Where(ur => ur.UserId == userId).Select(r => r.RoleId)
                    .ToList();

                result = allRoles.Where(r => userRoleIds.Contains(r.Id)).ToList();
            }

            return result;

        }

        public IdentityModels.ApplicationUser CreateUser(UserForCreateDto request)
        {
            var result = _userFactory.CreateUser(request);
            return result;
        }


        public ICollection<UserTypeBase> GetUserTypes()
        {

            var typeEntities = _dbContext.UserTypes.ToList();
            var result = Mapper.Map<List<UserTypeBase>>(typeEntities);
            return result;
        }

        public ICollection<UserStatusBase> GetUserStatuses()
        {
            var typeEntities = _dbContext.UserStatuses.ToList();
            var result = Mapper.Map<List<UserStatusBase>>(typeEntities);
            return result;
        }

        public ICollection<IdentityModels.ApplicationUser> GetUsersByType(int type)
        {
            var users = _dbContext.Users.Where(u => u.TypeId == type && u.DeletionTimeStamp == null).ToList();
            return users;
        }

        public ICollection<IdentityModels.ApplicationUser> GetUsersByStatus(int status)
        {

            var users = _dbContext.Users.Where(u => u.TypeId == status && u.DeletionTimeStamp == null).ToList();
            return users;

        }

        public bool DeleteUser(int userId)
        {

            var userToDelete = GetUserById(userId, _dbContext);

            if (userToDelete == null) return false;

            if (userToDelete.TypeId == (int)UserType.Executive || userToDelete.TypeId == (int)UserType.Admin)
                throw new ArgumentException(Core.Messages.Errors.SuchTimeOfUserCannotBeDeletedError);


            // The user is locked for an infinite time instead of changing the sign-in manager
            userToDelete.DeletionTimeStamp = DateTime.UtcNow.ToUnixTimestamp();
            userToDelete.LockoutEnabled = true;
            userToDelete.LockoutEndDateUtc = DateTime.MaxValue;

            var assignedRequests = _dbContext.Requests.Where(r =>
                r.AssigneeId == userToDelete.Id && r.StatusId != (int)RequestStatus.Closed);

            // If the user with the assigned request is deleted,
            // the requests are assigned to the executive

            if (assignedRequests.Any())
            {
                var executiveId = _dbContext.Users.First(u => u.TypeId == (int)UserType.Executive).Id;
                assignedRequests.ForEach(request =>
                {
                    request.AssigneeId = executiveId;
                });
            }

            return _dbContext.SaveChanges() > 0;

        }

        public string ChangeApiKey(int userId)
        {

            var userForUpdate = GetUserById(userId, _dbContext);

            if (userForUpdate != null)
            {
                userForUpdate.ApiKey = Randoms.ApiKey;
                userForUpdate.ApiKeyCreationTimeStamp = DateTime.UtcNow.ToUnixTimestamp();
                _dbContext.SaveChanges();
                return userForUpdate.ApiKey;
            }

            throw new Exception(Core.Messages.Errors.UpdateApiKeyError);
        }



        #region private methods
        private static IdentityModels.ApplicationUser GetUserById(int userId, ApplicationDataContext context)
        {

            return context.Users.SingleOrDefault(u => u.Id == userId
                                       && u.DeletionTimeStamp == null);
        }

        #endregion

    }
}