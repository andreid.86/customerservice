﻿using AutoMapper;
using Core.Enums;
using Core.Utils;
using Core.Utils.Extensions;
using DataAccess;
using Microsoft.AspNet.Identity;
using Services.Dtos;
using Services.Interfaces.UserService;
using System;
using System.Linq;

namespace Services.Implementations.UserService
{
    public sealed class UserFactory : IUserFactory
    {

        private readonly IMapper _mapper;
        private readonly ApplicationDataContext _dbContext;
        private readonly IdentityConfig.ApplicationUserManager _userManager;

        public UserFactory(IMapper mapper, ApplicationDataContext dbContext, IdentityConfig.ApplicationUserManager userManager)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _userManager = userManager;
        }

        public IdentityModels.ApplicationUser CreateUser(UserForCreateDto request)
        {
            if (!Enum.IsDefined(typeof(UserType), request.UserTypeId))
                throw new ArgumentException(Core.Messages.Errors.IncorrectUserType);

            var user = _mapper.Map<IdentityModels.ApplicationUser>(request);

                var result = _userManager.Create(user, request.Password);

                if (result != IdentityResult.Success)
                    throw new Exception(result.Errors.First());


                switch (request.UserTypeId)
                {
                    case (int)UserType.Admin:
                        {
                            _userManager.AddToRole(user.Id, Core.Constants.RoleNames.Administrator);
                            break;
                        }

                    case (int)UserType.Customer:
                        {
                            _userManager.AddToRole(user.Id, Core.Constants.RoleNames.Api);
                            user.ApiKey = Randoms.ApiKey;
                            user.ApiKeyCreationTimeStamp = DateTime.UtcNow.ToUnixTimestamp();
                            break;
                        }

                    case (int)UserType.Executive:
                        {
                            _userManager.AddToRole(user.Id, Core.Constants.RoleNames.Administrator);
                            break;
                        }

                    case (int)UserType.Manager:
                        {
                            break;
                        }

                    case (int)UserType.Support:
                        {
                            user.StatusId = (int)UserStatus.Free;
                            break;
                        }

                    default:
                        {
                            break;
                        }
                }

                _userManager.Update(user);

                return user;
        }
    }
}