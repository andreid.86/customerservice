﻿using System;
using Services.Interfaces;

namespace Services.Implementations.Loggers
{
    public sealed class ConsoleLoggerService : ILoggerService
    {
        public void LogInfo(string message)
        {
            Console.WriteLine($@"INFO: {message}");
        }

        public void LogError(string message)
        {
            Console.WriteLine($@"ERROR: {message}");
        }

        public void LogException(Exception ex)
        {
            Console.WriteLine($@"EXCEPTION: {ex.StackTrace}");
        }
    }
}