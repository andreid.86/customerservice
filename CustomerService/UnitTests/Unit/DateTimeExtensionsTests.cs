﻿using System;
using Core.Utils.Extensions;
using NUnit.Framework;

namespace UnitTests.Unit
{

    [TestFixture]
    public class DateTimeExtensionsTests
    {

        public class TestData
        {
            public DateTime Date { get; set; }
            public long ExpectedResult { get; set; }
        }

        private static TestData[] _testData = new[] {
            new TestData()
            {
                Date = new DateTime(2019, 10, 10, 0, 0, 0, DateTimeKind.Local),
                ExpectedResult = 1570654800
            },
            new TestData()
            {
                Date = new DateTime(2019, 10, 10, 0, 0, 0, DateTimeKind.Utc),
                ExpectedResult = 1570665600
            }
        };

        [Test]
        public void ToUnixTimestamp_WhenCalledShouldReturnUnixTimeStamp([ValueSource(nameof(_testData))] TestData testData)
        {
            Assert.AreEqual(testData.ExpectedResult, testData.Date.ToUnixTimestamp());
        }

    }
}
