﻿using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using DataAccess.BaseModels;
using NUnit.Framework;
using Services.Implementations.QueueService;
using Services.Interfaces.QueueService;
using Services.Interfaces.RequestService;

namespace UnitTests.Integration
{
    [TestFixture]
    public class QueueServiceTests
    {
        private IContainer _autofacContainer;

        protected IContainer AutofacContainer => _autofacContainer ?? (_autofacContainer = AutofacConfig.Register());

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Instance_Should_Return_Queue_With_Unprocessed_Requests()
        {

            using (var scope = AutofacContainer.BeginLifetimeScope())
            {

                var requestService = scope.Resolve<IRequestService>();

                var requestsLists = requestService.GetUnprocessedRequests();

                var queueService = scope.Resolve<IQueueService>();

                var queueLength = queueService.Length;

                Assert.That(queueLength == requestsLists.Count);
            }

        }

    }
}