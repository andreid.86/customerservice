﻿using Autofac;
using AutoMapper;
using DataAccess;
using Microsoft.AspNet.Identity.Owin;
using Services.AutoMapper;
using Services.Implementations.QueueService;
using Services.Implementations.RequestService;
using Services.Implementations.UserService;
using Services.Interfaces.QueueService;
using Services.Interfaces.RequestService;
using Services.Interfaces.UserService;

namespace UnitTests.Integration
{
    public static class AutofacConfig
    {
        public static IContainer Register()
        {
            var builder = new ContainerBuilder();

            // Register services
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<RequestService>().As<IRequestService>().InstancePerLifetimeScope();
            builder.RegisterInstance(QueueService.Instance).As<IQueueService>();

            // Register Db Context
            // builder.RegisterType<ApplicationDataContext>().InstancePerDependency();

            // Register Identity Managers
            // builder.RegisterType<IdentityConfig.ApplicationUserManager>().AsSelf().InstancePerLifetimeScope();
            // builder.RegisterType<IdentityConfig.ApplicationRoleManager>().AsSelf().InstancePerLifetimeScope();
            // builder.RegisterType<IdentityConfig.ApplicationSignInManager>().AsSelf().InstancePerLifetimeScope();
            // builder.Register(c => new UserStore<IdentityModels.ApplicationUser, IdentityModels.Role, int, IdentityModels.UserLogin, IdentityModels.UserRole,IdentityModels.UserClaim>(c.Resolve<ApplicationDataContext>())).AsImplementedInterfaces().InstancePerLifetimeScope();
            // builder.Register(c => new RoleStore<IdentityModels.Role, int, IdentityModels.UserRole>(c.Resolve<ApplicationDataContext>())).AsImplementedInterfaces().InstancePerLifetimeScope();
            
            builder.Register(c => new IdentityFactoryOptions<IdentityConfig.ApplicationUserManager>
            {
                DataProtectionProvider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("Application​")
            });


            // Creating an Instance for the Mapper
            builder.RegisterInstance(new AutoMapperConfiguration().Configure()).As<IMapper>();

            return builder.Build();

        }
    }
}